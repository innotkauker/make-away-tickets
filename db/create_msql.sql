CREATE TABLE `MakeAwayDB`.`planes`(
        `plane_id` int NOT NULL AUTO_INCREMENT,
        `title` varchar(50) NOT NULL,
 CONSTRAINT `PK_planes` PRIMARY KEY CLUSTERED (
        `plane_id` ASC
 )
)
;

CREATE TABLE `MakeAwayDB`.`airports`(
        `airport_id` int NOT NULL AUTO_INCREMENT,
        `title` varchar(50) NOT NULL,
        `city_id` int NOT NULL,
 CONSTRAINT `PK_airports` PRIMARY KEY CLUSTERED (
        `airport_id` ASC
 )
)
;

CREATE TABLE `MakeAwayDB`.`cities`(
        `city_id` int NOT NULL AUTO_INCREMENT,
        `name` varchar(50) NOT NULL,
        `country_id` int NOT NULL,
 CONSTRAINT `PK_cities` PRIMARY KEY CLUSTERED (
        `city_id` ASC
 )
)
;

CREATE TABLE `MakeAwayDB`.`companies`(
        `company_id` int NOT NULL AUTO_INCREMENT,
        `title` varchar(50) NOT NULL,
        `logo_url` varchar(300) NULL,
        `bonus_points_ratio` float NOT NULL,
        `bonus_points_activated` float NOT NULL,
        `sold_tickets_value` float NOT NULL,
 CONSTRAINT `PK_companies` PRIMARY KEY CLUSTERED (
        `company_id` ASC
 )
)
;

CREATE TABLE `MakeAwayDB`.`countries`(
        `country_id` int NOT NULL AUTO_INCREMENT,
        `name` varchar(50) NOT NULL,
        `flag_url` varchar(300) NOT NULL,
 CONSTRAINT `PK_countries` PRIMARY KEY CLUSTERED (
        `country_id` ASC
 )
)
;

CREATE TABLE `MakeAwayDB`.`flights`(
        `flight_id` int NOT NULL AUTO_INCREMENT,
        `company_id` int NOT NULL,
        `airport_out_id` int NOT NULL,
        `airport_in_id` int NOT NULL,
        `date` datetime NOT NULL,
        `length` int NOT NULL,
		`plane_id` int NOT NULL,
 CONSTRAINT `PK_flights` PRIMARY KEY CLUSTERED (
        `flight_id` ASC
 )
)
;

CREATE TABLE `MakeAwayDB`.`points`(
        `user_id` int NOT NULL,
        `company_id` int NOT NULL,
        `value` int NOT NULL,
 CONSTRAINT `PK_points` PRIMARY KEY CLUSTERED (
        `user_id` ASC,
        `company_id` ASC
 )
)
;

CREATE TABLE `MakeAwayDB`.`tickets`(
		`ticket_id` int NOT NULL AUTO_INCREMENT,
        `user_id` int NULL,
        `flight_id` int NOT NULL,
        `seat` int NOT NULL,
        `type` int NOT NULL,
        `cost` float NOT NULL,
        `paid` float NOT NULL,
        `payment_secret` varchar(20) NULL,
        `destination_wallet` varchar(35) NULL,
 CONSTRAINT `PK_tickets` PRIMARY KEY CLUSTERED (
        `ticket_id` ASC
 )
)
;

CREATE TABLE `MakeAwayDB`.`users`(
        `user_id` int NOT NULL AUTO_INCREMENT,
        `email` varchar(50) NOT NULL,
        `name` varchar(50) NOT NULL,
        `last_name` varchar(50) NOT NULL,
        `patronymic` varchar(50) NULL,
        `password_hash` varchar(128) NOT NULL,
        `last_login` datetime NULL,
        `date_registered` datetime NOT NULL,
 CONSTRAINT `PK_users` PRIMARY KEY CLUSTERED (
        `user_id` ASC
 )
)
;

ALTER TABLE `MakeAwayDB`.`airports` 
        ADD  CONSTRAINT `FK_airports_cities` FOREIGN KEY(`city_id`)
        REFERENCES `MakeAwayDB`.`cities` (`city_id`)
;


ALTER TABLE `MakeAwayDB`.`cities` 
        ADD  CONSTRAINT `FK_cities_countries` FOREIGN KEY(`country_id`)
        REFERENCES `MakeAwayDB`.`countries` (`country_id`)
;


ALTER TABLE `MakeAwayDB`.`flights` 
        ADD  CONSTRAINT `FK_flights_planes` FOREIGN KEY(`plane_id`)
        REFERENCES `MakeAwayDB`.`planes` (`plane_id`)
;


ALTER TABLE `MakeAwayDB`.`flights` 
        ADD  CONSTRAINT `FK_flights_airports` FOREIGN KEY(`airport_out_id`)
        REFERENCES `MakeAwayDB`.`airports` (`airport_id`)
;


ALTER TABLE `MakeAwayDB`.`flights` 
        ADD  CONSTRAINT `FK_flights_airports1` FOREIGN KEY(`airport_in_id`)
        REFERENCES `MakeAwayDB`.`airports` (`airport_id`)
;


ALTER TABLE `MakeAwayDB`.`flights` 
        ADD  CONSTRAINT `FK_flights_companies` FOREIGN KEY(`company_id`)
        REFERENCES `MakeAwayDB`.`companies` (`company_id`)
;


ALTER TABLE `MakeAwayDB`.`points` 
        ADD  CONSTRAINT `FK_points_companies` FOREIGN KEY(`company_id`)
        REFERENCES `MakeAwayDB`.`companies` (`company_id`)
;


ALTER TABLE `MakeAwayDB`.`points` 
        ADD  CONSTRAINT `FK_points_users` FOREIGN KEY(`user_id`)
        REFERENCES `MakeAwayDB`.`users` (`user_id`)
;


ALTER TABLE `MakeAwayDB`.`tickets` 
        ADD  CONSTRAINT `FK_tickets_flights` FOREIGN KEY(`flight_id`)
        REFERENCES `MakeAwayDB`.`flights` (`flight_id`)
;


ALTER TABLE `MakeAwayDB`.`tickets` 
        ADD  CONSTRAINT `FK_tickets_users` FOREIGN KEY(`user_id`)
        REFERENCES `MakeAwayDB`.`users` (`user_id`)
;


