INSERT INTO `planes` (`plane_id`, `title`) VALUES (1, 'UWMMIXNkeEXveTMSuBIY');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (2, 'bnopPLiiEzSvpgCturGh');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (3, 'MyhfMwcfYXcIhEBvGNqr');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (4, 'AaNbgkrZHqkazsIdZdaE');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (5, 'paanaWQqPAZRvQdpuQjn');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (6, 'fXAxAvuYJsSrooCsNzqU');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (7, 'mPOAAFWxUFMeBQHumeQX');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (8, 'OeIntvHeAlGHMYMEAUyB');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (9, 'oZzskUewezllUUlBsgqs');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (10, 'blhaJCuOBwZhiutosdUh');
INSERT INTO `planes` (`plane_id`, `title`) VALUES (11, 'PQuecbyXrxmudTnipSfc');

INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (1, 'OQoQCrUrwSyTCeacHtXz', 'http://aCEAs.nWL/BjkoiVxVOw');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (2, 'wVhiLMFOjDlyXyggpndi', 'http://xnLdx.llL/gkRVZVDulq');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (3, 'sHhPjOpGkJyMMJmRwEOv', 'http://zTnxx.dtI/YKDqFlepxz');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (4, 'ScuasnYSoBMSpWHFIxec', 'http://XWFgV.Cqt/fRfJXjRaOn');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (5, 'wyEGjEOWLqUcPgGbszha', 'http://gYFRY.oWR/GxhkBbtkoW');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (6, 'UDekLggTtrobgAJidXkI', 'http://RjDZb.NPW/SPPQyBIoLr');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (7, 'xqBLbItiFJBKbKxVBvtL', 'http://BzajZ.fuQ/RNftZijycq');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (8, 'fIJyTfweTOGlyJNNGZJP', 'http://SXVFO.EpF/yNFTaKhTTZ');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (9, 'ZwjWYCTvFvHPJEJenSYZ', 'http://faLnq.egE/AzdHFeuoEm');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (10, 'aUSZFZlnxVOdfetBndKv', 'http://ONFSf.tsR/VEyWnbRUkn');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (11, 'BzbpwlxAAmFRUStGitOd', 'http://dnCHw.vTC/xbilSldJbq');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (12, 'ppZyzJWjSzlmChyrhioI', 'http://uqibg.XPe/jMqKLUQwbq');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (13, 'ovSVQRFVdeByivyjOGnd', 'http://hCNCs.VLf/fHlMoetvWE');
INSERT INTO `countries` (`country_id`, `name`, `flag_url`) VALUES (14, 'MMOHSNgrZHBKNodOLrzr', 'http://cOeyi.zdl/LWgXoxenDE');

INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (1, 'fDYtaFXIPeFmQgBJSwEp', 5);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (2, 'HyRYjuywTnXCVCWZxjtQ', 7);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (3, 'skdKSobdKNzDatYsfVuT', 4);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (4, 'jBRovsyZSpOYLvPEUlRl', 3);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (5, 'VZcWifVisSysmXdhDAVm', 4);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (6, 'rMCyqhiKCeLlTKkFfCId', 2);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (7, 'ADorGfnEAWuhyzMEmjtY', 6);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (8, 'xSXlBEbEagPNvCnHzLWC', 3);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (9, 'LSutGTGOWxeAtYSagXMg', 4);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (10, 'OZBZFmOeJEEqkFMuzXrm', 3);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (11, 'TgzCAqXcMxstdZrGvxRt', 1);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (12, 'bjecrJZNQZXcFchWjxNa', 2);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (13, 'HLKsfzsFYbflvCxjSetO', 6);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (15, 'XVABSdTtyxxMnFDiEuKT', 6);
INSERT INTO `cities` (`city_id`, `name`, `country_id`) VALUES (16, 'JfXXhIdgpFrGiTWemHpp', 5);

INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (1, 'uIRnPifMMqBTCWBWASFG', 1);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (2, 'BxcppkEBVHqkNgRxPYEO', 6);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (3, 'izjSyhjqyLpUoisVzmBu', 1);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (4, 'XPKyKMMjzKvseHuUsPGT', 2);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (5, 'KBVYdFnXsstbdPvbHlvm', 4);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (6, 'zOittaUpFRtDwFiiiAZZ', 2);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (7, 'zRyomXmaMaeGqcXGTzXz', 4);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (8, 'cWBLMLyTNcWxwJkjeovq', 7);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (9, 'foyKnIJlbduDpFJqHRxO', 3);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (10, 'mBHOtipdWKsYhmpbjyeM', 5);
INSERT INTO `airports` (`airport_id`, `title`, `city_id`) VALUES (11, 'ScpZugmJkNMBYCbvpErm', 4);



INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (1, 'icGWMUVwpCpZJPSKbggS', 'http://image.stirileprotv.ro/media/images/620xX/Dec2008/60210975.jpg', 0.8771394104775112, 17341.268151888034, 69724.94645863713);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (2, 'PTYxiKwemFtshGKkkGnE', 'http://www.antena3.ro/fotonew/crizabestbuy.jpg', 0.6408813801858044, 84363.52170281376, 32832.50172880272);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (3, 'hIKHnjTTzOTjMQMbsvKr', 'http://best-dem.ru/wp-content/uploads/2011/11/Image00007160.jpg', 0.6089895243028771, 17104.69252506287, 10871.058086358044);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (4, 'SxBPnHWcfBnGcpoKskIF', 'http://vitamin-e.laclinique.biz/sites/default/files/pictures/blog/auto_bmw.png', 0.13773917609490438, 99856.6169315665, 69194.82319952814);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (5, 'JgjlaGcAtZMuaPRwXqnm', 'http://vitamin-e.laclinique.biz/sites/default/files/pictures/blog/auto_bmw.png', 0.5857420983470795, 33808.34695198786, 43397.81776420387);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (6, 'nKRBNYKESSwNoInbgFwg', 'http://www.nafb.com/nafbfiles/MidwestAirlinesSignaturewithTagLine.jpg', 0.6147372724094204, 38904.11077253442, 93705.6007850771);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (7, 'TeKNRIFMKifYJgIvgqTV', 'http://th182.photobucket.com/albums/x7/Sherman_36/th_Royals.jpg', 0.9203297070088643, 96930.11862659772, 4030.4530495096747);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (8, 'ggXYyMgQZevVrFITxirx', 'http://www.vmeste.eu/news/2012-06/1340958719.jpg', 0.32990016628641583, 73237.72427762224, 90674.42946527844);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (9, 'mBCkNbGBoVBobOdhvrWk', 'http://www.asiamarketing.cl/img/clientes/lays.png', 0.29371452759005123, 37752.63989780944, 66978.24962725527);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (10, 'bZidXWKBNWwvzGtdzglf', 'http://www.inploid.com/Embedded/images/45201215055702.jpg', 0.017730475508697108, 18138.552978612443, 68948.71687103875);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (11, 'CxcmddAYHYQUFsunarbX', 'http://FciWM.bBJ/DtFmdsUbeZ', 0.06095064481000645, 52423.71173589865, 25624.158562624212);
INSERT INTO `companies` (`company_id`, `title`, `logo_url`, `bonus_points_ratio`, `bonus_points_activated`, `sold_tickets_value`) VALUES (12, 'KLQjLkoqRlbhKSCeyXrU', 'http://UmHlt.Qdj/wsVaVpPSCJ', 0.1285092489104247, 94621.9343744891, 59765.69008234218);


INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (1, 5, 9, 8, '2009-09-19 18:40:33', 39051.868707668255, 6);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (2, 6, 8, 3, '1977-02-05 10:20:25', 75682.58140167242, 6);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (3, 9, 6, 4, '2010-07-22 07:55:44', 34396.9845203677, 2);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (4, 2, 6, 4, '2009-03-03 07:26:00', 99114.77681112592, 7);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (5, 7, 2, 1, '1993-12-04 20:54:50', 49474.78796737511, 9);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (6, 6, 5, 2, '1990-01-27 16:54:17', 46349.73752592001, 8);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (7, 4, 5, 4, '2006-03-14 14:37:53', 9652.158699552027, 6);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (8, 5, 5, 7, '1991-01-08 23:17:47', 90627.46071135261, 5);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (9, 4, 9, 8, '1989-09-24 04:15:38', 5201.875889233099, 2);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (10, 2, 8, 2, '1990-08-12 20:08:49', 20871.756778397397, 3);
INSERT INTO `flights` (`flight_id`, `company_id`, `airport_out_id`, `airport_in_id`, `date`, `length`, `plane_id`) VALUES (11, 5, 6, 5, '1998-07-31 21:15:49', 75287.01104941504, 2);

INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (1, 'SdMca@jfcII.yHa', 'gFeFPBLMdyNEckcLsufy', 'ArViXBJiTlSwvvdpMNRV', 'IbmerawZIYVTOomvXCay', 'fyQqKYbEzVeZcmDSDHIX', '2009-07-19 07:21:30', '2010-09-26 23:09:58');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (2, 'anIco@kFDFa.OGX', 'RoVuiZBhiarsBeuAykjy', 'vgKVXfFxJMvPRiYSegzq', 'ykxDwzttOtDzmsamPHqD', 'NKcUiaVOdFeBCwhMWJus', '1987-02-09 07:37:43', '1975-03-03 11:07:16');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (3, 'gCJav@ouXfA.qRv', 'ztrMUWMrHUPTLDmZfSUQ', 'lGppizAxlWAEWJzrZduC', 'gUFMtPKlkOZuBAsddNFx', 'DnRIqLkepjOpiLgkAEeG', '2007-09-19 03:15:20', '1984-12-05 17:19:06');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (4, 'gFxJx@xjZuh.KGh', 'AQUDBefphprPVIupPcpq', 'cVyNvHxpxRCahDCOHNFA', 'MudUmRpEmyHiiRYufSUq', 'qgsUsduGNynXZqiBsaBk', '1972-02-28 17:19:58', '2006-08-31 01:54:41');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (5, 'JckQr@TyDwV.hTy', 'cVvDjvRtxHDCgdtGnQCV', 'vVeMwjbSegyAdRtiWRIh', 'WRZCrhCFpTdBDoFnmrIh', 'zDBjlzKrXASASxnsVoVp', '1988-09-07 20:00:27', '2002-02-22 03:07:03');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (6, 'eNSDC@kiaEB.SkJ', 'FhSHnmbLWNjPUuVfHShd', 'KYgdshwHOYWYxElwVyVt', 'urkyoEMBzPabtDfGZVvL', 'gZKepvOMxnNOetUgcRGp', '1977-11-21 22:18:58', '2007-01-28 14:25:37');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (7, 'BuRkr@nALbr.ZVs', 'YKEcfJTmIzURJUlJZiNl', 'OwNGxJRTJoHieXtwRkzV', 'pkbdkPgUTZokoBYmDTjR', 'sVpxaCWYjBPFccNMncoA', '1978-04-01 10:05:19', '2009-01-30 03:39:40');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (8, 'itRIh@fheVt.Bus', 'qrVbUiGLykPcqvZqnVBq', 'fdtzVkjIbCPjHwdCHvrO', 'drzPuMTcTIrMTeRlOlxQ', 'hLrvTxdUAQJMuZDxPnuv', '1987-12-02 07:51:10', '1998-11-20 23:48:24');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (9, 'XGdKN@mXSos.Ofv', 'aZtpeOtQtQDcrBhZNEol', 'zAPAyieSfXFjraSTMaYs', 'puwyvHqyuZrPNynACZBQ', 'nncKQzgcASMgriUlmzFJ', '1981-01-06 18:00:09', '1978-10-02 14:36:31');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (10, 'GhMsi@yztxI.MOQ', 'sVxzhiLYEIsbfVgXwhpQ', 'joexgzqtOMOdHGAvuWaD', 'jPmvDPswbfVqceOIRBmL', 'gNVDxcMWVNSXDpjymhqo', '2008-07-06 23:57:17', '1994-10-24 14:05:45');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (11, 'WCLHV@xfpPc.CYs', 'cxlYWOrbWOmLjiKuUieT', 'lVwuYQstRmGViiJiUmZl', 'IdrUynmfuXALpJfqUBjD', 'FLwvrPewRNzIcossXdLj', '1986-09-05 01:01:15', '2008-02-14 20:51:28');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (12, 'ivdYJ@KPDMc.eFg', 'uywSGanYpaDxFYrYisLb', 'LixMSOeiEOQVUbPPjabn', 'wpeqmeVLsOFVXONrRsqu', 'llLpDmPPMywKvYxxNwqp', '1971-08-27 09:47:51', '1991-07-27 11:07:18');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (13, 'LIQmt@VLgfp.mFP', 'PkDxrMUKqXmyQKgBuaJc', 'nvdVUWuEehADBYPLfdZw', 'yhebEPLavEoFAybyylDE', 'jmNpoXunBkznXcDmPwjM', '1981-08-09 07:38:50', '1983-12-06 19:12:12');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (14, 'ebaQh@pOARm.Zfi', 'mSvOurQZTvluqXtqQNUh', 'QFCNWcrPJRiZlQPtWCEp', 'ONCiYewpJAMNFhFZkmeG', 'uGITPHDsVoWrziaoSZYt', '1978-10-07 22:58:35', '2006-06-20 01:28:21');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (15, 'JYlee@SBLrR.UDb', 'fCRvkQcvNSHVyVrnoCLp', 'jpKFvPFFnYVgOUFoUkBK', 'dWrXMeRVXfooXpUTLAKz', 'BtzLpabAJjXPOJUoiffJ', '2001-03-12 22:37:39', '2013-09-22 18:49:52');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (16, 'dxZGR@Yevha.Rzq', 'YdOkCParbFdyQyWtUYQU', 'UmtiAEQkeEPrHjfYUhEy', 'wfNCTwSOWlveCKtcFtOK', 'MxqTOgEUBHDPddbNjmxa', '2009-03-02 18:48:38', '1971-06-09 13:03:46');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (17, 'FwneY@ZlFtF.mue', 'bUQwZGoBzrPjoYhshJTF', 'vPpObLJzvPwZyhadrYWx', 'ukdbwNvHoPtkoJRAtbIl', 'hFWwCieEkQieHJNBVtsT', '1996-05-04 19:12:18', '1986-03-03 07:35:11');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (18, 'rsLBg@LBlrZ.ZGn', 'GmZOpPmbuVMEfuzyNHUL', 'JZXMullqItASBhmwgDIx', 'FVCyzPFyXGGueyRSLZtb', 'QzfrSYmCgFnkPfUERGVW', '1975-12-16 22:30:13', '1998-01-30 22:08:22');
INSERT INTO `users` (`user_id`, `email`, `name`, `last_name`, `patronymic`, `password_hash`, `last_login`, `date_registered`) VALUES (19, 'Ihfxw@iOMTV.ThK', 'AhIlNOsORMVgUnsbMJWW', 'relHvUyXaUQQMyDHfYzA', 'qtaurARShVAldIhOPtTl', 'xkJXOrIsqwzqPgGdPhms', '2002-04-02 08:29:11', '2013-06-13 09:03:56');

INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (1, 3, 74744.65966494543);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (2, 6, 26134.267096346863);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (3, 6, 30865.45351778771);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (4, 7, 23669.327430796162);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (5, 8, 57891.10260683911);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (6, 4, 69207.65248065988);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (7, 6, 23360.481507773056);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (8, 2, 55761.72651297402);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (9, 8, 92831.0019185065);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (10, 7, 21326.766453585533);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (11, 4, 72574.39881864059);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (12, 9, 31963.812063603382);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (13, 3, 69058.62495017919);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (14, 8, 61739.07200280336);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (15, 1, 35798.67428463218);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (16, 7, 22561.572817980046);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (17, 2, 80218.74576401655);
INSERT INTO `points` (`user_id`, `company_id`, `value`) VALUES (18, 4, 97305.76553278408);

INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (1, 27, 4, 9, 2, 0.65, 0, 'gCxntgaOICeociIzalRp', 'pbhWswPjYiUBoXcrtdBi');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (2, null, 7, 4, 2, 0.75, 0, 'edXGOShnfrtJYgWZFvPW', 'bxxXWdGLBYUFeExLlEYq');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (3, null, 5, 2, 1, 1.55, 0, 'iGnpZHSntcLfWfSymxqH', 'gcAfKezjVUMZOTtCpDGp');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (4, null, 2, 8, 2, 0.58, 0, 'fwpjBSKupvHznuHspQbU', 'JuOKExJaWAxhFoGpUhpJ');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (5, null, 4, 7, 2, 0.05, 0, 'asTySMYNlpchnZtSuVuR', 'CZBiLheMIdRLljjzAptl');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (6, null, 1, 6, 2, 0.35, 0, 'iITgXNGsjGVUweiDZGQa', 'uNHQrjfjLrjHgtRxhRNO');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (7, null, 9, 1, 2, 0.4535, 0, 'SBcQZDRnobdKUHfUaZoJ', 'xVNTxYCoHZJCNyUwFjrH');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (8, null, 8, 7, 2, 3.55, 0, 'jlpcaXVSMretVZzwGSiG', 'BMVhpNDRsZlLxKTTGxlp');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (9, null, 8, 3, 2, 6.55, 0, 'FtEiukWieImntVvmYhHk', 'ZjRffvAaFJYxTdHsJPjg');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (10, null, 7, 2, 1, 0.55, 0, 'GBuWaODGHbZDrmiWVOmI', 'VdfPvKOzKlrLfBFleltX');
INSERT INTO `tickets` (`ticket_id`, `user_id`, `flight_id`, `seat`, `type`, `cost`, `paid`, `payment_secret`, `destination_wallet`) VALUES (11, null, 3, 9, 2, 1.05, 0, 'eLgpQxsYsodJpIWTNXgt', 'UFBnqoiLudGMvFpfBJuU');
