package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.AirportsDAO;
import org.badcoding.hibernate.stored.Airports;
import org.badcoding.hibernate.logic.Database;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

@Service
public class AirportsDAOImplementation implements AirportsDAO {
public void add(Airports airport) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.save(airport);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void update(Airports airport) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(airport);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Airports airport) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(airport);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public Airports getById(int id) {
	Session session = null;
	Airports airport = null;
	try {
		session = Database.getFactory().openSession();
		airport = (Airports)session.get(Airports.class, id);
	} finally {
		if (session != null && session.isOpen())
  			session.close();
	}
	return airport;
}



}
