package org.badcoding.dao.implementation;

import org.apache.commons.io.IOUtils;
import org.json.simple.JSONValue;
import org.badcoding.dao.interfaces.*;

import java.lang.Exception;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Value;

@Service
public class BlockchainAPIimplementation implements BlockchainAPI {
	private final String blockchain = "https://blockchain.info/";

	@Value("${application.callback}")
    private String callback_url;

	@Value("${application.address}")
    private String address;

	public String getAddress() {
		return address;
	}

    private String fetchURL(String URL) throws Exception {
        URL url = new URL(URL);

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        connection.setConnectTimeout(10000);
        connection.setReadTimeout(10000);

        connection.setInstanceFollowRedirects(false);

        connection.connect();

        if (connection.getResponseCode() != 200) {
            throw new Exception(IOUtils.toString(connection.getErrorStream(), "UTF-8"));
        }

        return IOUtils.toString(connection.getInputStream(), "UTF-8");
    }

/**
* Generate a Unique payment address for a User to send payment to
* @param myAddress Your bitcoin address
* @param callback The callback URL which will be notified when a payment is received
* @param anonymous Whether the transaction should be anonymous or not
* @return
* @throws Exception
*/
    public String generatePaymentAddress(int ticket_id, String secret, boolean anonymous) throws Exception {
		String callback = callback_url + secret + "&ticket_id=" + ticket_id;
        String url = blockchain + "api/receive?method=create" + "&address=" + address + "&callback=" + URLEncoder.encode(callback, "UTF-8");

        String response = fetchURL(url);

        if (response == null)
            throw new Exception("Server Returned NULL Response");

        @SuppressWarnings("unchecked")
		Map<String, Object> obj = (Map<String, Object>) JSONValue.parse(response);

        if (obj.get("error") != null)
            throw new Exception((String) obj.get("error"));

        return (String)obj.get("input_address");
    }
}
