package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.CitiesDAO;
import org.badcoding.hibernate.stored.Cities;
import org.badcoding.hibernate.logic.Database;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;
import org.hibernate.Query;
import org.hibernate.Session;

@Service
public class CitiesDAOImplementation implements CitiesDAO {
public void add(Cities city) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.save(city);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void update(Cities city) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(city);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Cities city) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(city);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public Cities getById(int id) {
	Session session = null;
	Cities city = null;
	try {
		session = Database.getFactory().openSession();
		city = (Cities)session.get(Cities.class, id);
	} finally {
		if (session != null && session.isOpen())
  			session.close();
	}
	return city;
}

@SuppressWarnings("unchecked")
public List<Cities> getByTitle(String title) {
	Session session = null;
	List<Cities> result = new ArrayList<Cities>();
	try {
		String hsql_query = 
			" select c "
			+ " from Cities c";
		if (title != null)
			hsql_query += " where c.name like '%' || :title || '%'";
		session = Database.getFactory().openSession();
    	session.beginTransaction();
		Query query = session.createQuery(hsql_query);
		if (title != null)
			query.setString("title", title);
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}

}
