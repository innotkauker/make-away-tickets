package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.CountriesDAO;
import org.badcoding.hibernate.stored.Countries;
import org.badcoding.hibernate.logic.Database;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

@Service
public class CountriesDAOImplementation implements CountriesDAO {
public void add(Countries country) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.save(country);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void update(Countries country) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(country);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Countries country) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(country);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public Countries getById(int id) {
	Session session = null;
	Countries country = null;
	try {
		session = Database.getFactory().openSession();
		country = (Countries)session.get(Countries.class, id);
	} finally {
		if (session != null && session.isOpen())
  			session.close();
	}
	return country;
}



}
