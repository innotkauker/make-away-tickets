package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.FlightsDAO;
import org.badcoding.hibernate.stored.Cities;
import org.badcoding.hibernate.stored.Companies;
import org.badcoding.hibernate.stored.Flights;
import org.badcoding.hibernate.logic.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.hibernate.Query;
import org.hibernate.Session;

@Service
public class FlightsDAOImplementation implements FlightsDAO {
public void add(Flights flight) {
	Session session = null;
	if (flight.getDate() == null) {
		flight.setDate(new Date());
	}
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.save(flight);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void update(Flights flight) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(flight);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Flights flight) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(flight);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public Flights getById(int id) {
	Session session = null;
	Flights flight = null;
	try {
		session = Database.getFactory().openSession();
		flight = (Flights)session.get(Flights.class, id);
	} finally {
		if (session != null && session.isOpen())
  			session.close();
	}
	return flight;
}

	@SuppressWarnings("unchecked")
	public List<Flights> getByChoice(Date start, Date end, Cities city_out, Cities city_in, Companies company, Boolean has_tickets) {
 	// System.out.println((start == null) + " " +  (end == null) + " " + (airport_out == null) + " " +  (airport_in == null) + " " +  (company == null));
	Session session = null;
	List<Flights> result = new ArrayList<Flights>();
	try {
		String hsql_query = 
				"select f " +
				"from Flights f ";
		if (city_in != null)
			hsql_query += ", Airports ai ";
		if (city_out != null)
			hsql_query += ", Airports ao ";

		// dates
		if (start == null && end != null)
			start = new Date();
		if (start != null && end != null)
			hsql_query += "where f.date between :start and :end ";
		else if (start != null && end == null)
			hsql_query += "where f.date > :start ";
		else 
			hsql_query += "where 1 = 1 ";
		
		// cities
		if (city_in != null)
			hsql_query += "and ai.city = :in_id and f.airport_in = ai.airport_id ";
		if (city_out != null)
			hsql_query += "and ao.city = :out_id and f.airport_out = ao.airport_id ";
	
		// company
		if (company != null)
			hsql_query += "and f.company = :company_id ";

		if (has_tickets)
			hsql_query += "and (select count(t.ticket_id)" +
		   			" from Tickets t " +
					" where t.flight = f.flight_id and " +
					" t.user = null) > 0 ";
		
		session = Database.getFactory().openSession();
    	session.beginTransaction();
		Query query = session.createQuery(hsql_query);
		if (start != null)
			query.setDate("start", start);
		if (end != null)
			query.setDate("end", end);
		if (city_in != null)
			query.setInteger("in_id", city_in.getCity_id());
		if (city_out != null)
			query.setInteger("out_id", city_out.getCity_id());
		if (company != null)
			query.setInteger("company_id", company.getCompany_id());
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}



}
