package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.PlanesDAO;
import org.badcoding.hibernate.stored.Planes;
import org.badcoding.hibernate.logic.Database;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

@Service
public class PlanesDAOImplementation implements PlanesDAO {
public void add(Planes plane) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.save(plane);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void update(Planes plane) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(plane);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Planes plane) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(plane);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public Planes getById(int id) {
	Session session = null;
	Planes plane = null;
	try {
		session = Database.getFactory().openSession();
		plane = (Planes)session.get(Planes.class, id);
	} finally {
		if (session != null && session.isOpen())
  			session.close();
	}
	return plane;
}



}
