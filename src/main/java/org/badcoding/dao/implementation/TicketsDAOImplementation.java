package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.TicketsDAO;
import org.badcoding.hibernate.stored.Flights;
import org.badcoding.hibernate.stored.Tickets;
import org.badcoding.hibernate.stored.Users;
import org.badcoding.hibernate.logic.Database;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.springframework.stereotype.Service;

@Service
public class TicketsDAOImplementation implements TicketsDAO {
public void add(Tickets ticket) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.save(ticket);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void update(Tickets ticket) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(ticket);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Tickets ticket) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(ticket);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public Tickets getById(int id) {
	Session session = null;
	Tickets ticket = null;
	try {
		session = Database.getFactory().openSession();
		ticket = (Tickets)session.get(Tickets.class, id);
	} finally {
		if (session != null && session.isOpen())
  			session.close();
	}
	return ticket;
}

@SuppressWarnings("unchecked")
public List<Tickets> getByUser(Users user, Boolean paid) {
	Session session = null;
	List<Tickets> result = new ArrayList<Tickets>();
	try {
		String hsql_query = 
				"select t " +
				"from Tickets t " +
				"where t.user = :user_id ";
		if (paid != null)
			if (paid == true)
				hsql_query += "and t.cost <= t.paid";
			else
				hsql_query += "and t.cost > t.paid";
		session = Database.getFactory().openSession();
    	session.beginTransaction();
		Query query = session.createQuery(hsql_query)
				.setInteger("user_id", user.getUser_id());
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}

@SuppressWarnings("unchecked")
public List<Tickets> getByFlight(Flights flight, Integer type) {
	Session session = null;
	List<Tickets> result = new ArrayList<Tickets>();
	try {
		String hsql_query = 
				"select t " +
				"from Tickets t " +
				"where t.flight = :flight_id " +
				"and t.user = null ";
		if (type != null)
			hsql_query += "and t.type = :type ";
		session = Database.getFactory().openSession();
    	session.beginTransaction();
		Query query = session.createQuery(hsql_query)
				.setInteger("flight_id", flight.getFlight_id());
		if (type != null)
			query.setInteger("type", type);
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}

@SuppressWarnings("unchecked")
public List<Tickets> getByFlight(Flights flight) {
	Session session = null;
	List<Tickets> result = new ArrayList<Tickets>();
	try {
		String hsql_query = 
				"select t " +
				"from Tickets t " +
				"where t.flight = :flight_id ";
		session = Database.getFactory().openSession();
    	session.beginTransaction();
		Query query = session.createQuery(hsql_query)
				.setInteger("flight_id", flight.getFlight_id());
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}

}
