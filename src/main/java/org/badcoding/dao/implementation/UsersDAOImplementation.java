package org.badcoding.dao.implementation;

import org.badcoding.dao.interfaces.UsersDAO;
import org.badcoding.hibernate.stored.Users;
import org.badcoding.hibernate.logic.Database;

import java.util.ArrayList;
import java.util.List;
import java.util.Date;

import org.springframework.stereotype.Service;
import org.hibernate.Query;
import org.hibernate.Session;

@Service
public class UsersDAOImplementation implements UsersDAO {
public void add(Users user) {
	Session session = null;
	if (user.getDate_registered() == null) {
		Date date = new Date();
		user.setDate_registered(date);
	}
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.save(user);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void update(Users user) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.update(user);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public void delete(Users user) {
	Session session = null;
	try {
		session = Database.getFactory().openSession();
		session.beginTransaction();
		session.delete(user);
		session.getTransaction().commit();
	} finally {
		if (session != null && session.isOpen())
			session.close();
	}
}

public Users getById(int id) {
	Session session = null;
	Users user = null;
	try {
		session = Database.getFactory().openSession();
		user = (Users)session.get(Users.class, id);
	} finally {
		if (session != null && session.isOpen())
  			session.close();
	}
	return user;
}

@SuppressWarnings("unchecked")
public List<Users> getByEmail(String email) {
	Session session = null;
	List<Users> result = new ArrayList<Users>();
	try {
		String hsql_query = 
				"select u " +
				"from Users u " +
				"where u.email = :email";

		session = Database.getFactory().openSession();
    	session.beginTransaction();
    	Query query = session.createQuery(hsql_query);
    	
		query.setString("email", email);
    	
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}

@SuppressWarnings("unchecked")
public List<Users> getByFilters(String first_name, String last_name, String patronymic, int flight, int company, int paid, String email) {
	Session session = null;
	List<Users> result = new ArrayList<Users>();
	try {
		String hsql_query = 
				"select u " +
				"from Users u ";

		if (first_name != null)
			hsql_query += "where u.name like '%' || :first_name || '%' ";
		else
			hsql_query += "where 1 = 1 ";
		if (last_name != null)
			hsql_query += "and u.last_name like '%' || :last_name || '%' ";
		if (patronymic != null)
			hsql_query += "and u.patronymic like '%' || :patronymic || '%' ";
		if (email != null)
			hsql_query += "and u.email like '%' || :email || '%' ";

		if (company != -1 || flight != -1 || paid != -1)
			hsql_query += "and " + (paid == 1 ? "not" : "") + " exists ( select t " +
				"from Users u0, Tickets t "; 
		if (company != -1 || flight != -1)
			hsql_query += ", Flights f ";
		if (company != -1)
			hsql_query += ", Companies c ";


		if (company != -1 || flight != -1 || paid != -1)
			hsql_query += "where u0.user_id = t.user and u0.user_id = u.user_id "; 
		if (company != -1 || flight != -1)
			hsql_query += "and f.flight_id = t.flight ";
		if (company != -1)
			hsql_query += "and c.company_id = f.company ";

		if (company != -1)
			hsql_query += "and c.company_id = '' || :company_id || '' ";
		
		if (flight != -1)
			hsql_query += "and f.flight_id = '' || :flight_id || '' ";

		if (paid == 1 || paid == 0)
			hsql_query += "and t.cost > t.paid ";
		if (company != -1 || flight != -1 || paid != -1)
			hsql_query += ") "; 
		
		
		session = Database.getFactory().openSession();
    	session.beginTransaction();
    	Query query = session.createQuery(hsql_query);
    	
		if (first_name != null)
			query.setString("first_name", first_name);
		if (last_name != null)
			query.setString("last_name", last_name);
		if (patronymic != null)
			query.setString("patronymic", patronymic);
		if (email != null)
			query.setString("email", email);
		if (flight != -1)
			query.setInteger("flight_id", flight);
		if (company != -1)
			query.setInteger("company_id", company);
    	
		System.out.println("query: " + hsql_query);
		result = query.list();
		session.getTransaction().commit();
	} finally {
    	if (session != null && session.isOpen()) {
    		session.close();
    	}
	}
	return result;
}


}
