package org.badcoding.dao.interfaces;

import java.util.List;

import org.badcoding.hibernate.stored.Companies;

public interface CompaniesDAO {
	public void add(Companies company);
	public void update(Companies company);
	public void delete(Companies company);
	public Companies getById(int id);
	
	// set integer parameters to -1 for no filtering
	public List<Companies> getByFilters(String title, int activated_bonus_gt, int sold_tickets_gt, int flights);
}
