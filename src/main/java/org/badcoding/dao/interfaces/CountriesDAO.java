package org.badcoding.dao.interfaces;

import org.badcoding.hibernate.stored.Countries;

public interface CountriesDAO {
	public void add(Countries country);
	public void update(Countries country);
	public void delete(Countries country);
	public Countries getById(int id);
}
