package org.badcoding.dao.interfaces;

import java.util.List;
import java.util.Date;

import org.badcoding.hibernate.stored.Cities;
import org.badcoding.hibernate.stored.Companies;
import org.badcoding.hibernate.stored.Flights;

public interface FlightsDAO {
	public void add(Flights flight);
	public void update(Flights flight);
	public void delete(Flights flight);
	public Flights getById(int id);
	
	// all parameters are nullable
	public List<Flights> getByChoice(Date start, Date end, Cities city_out, Cities city_in, Companies company, Boolean has_tickets);
}
