package org.badcoding.dao.interfaces;

import java.util.List;

import org.badcoding.hibernate.stored.Points;

public interface PointsDAO {
	public void add(int user_id, String code);
	public void update(Points point);
	public void delete(Points point);
	
	public List<Points> getByUser(int id, boolean show_zeroes);
	public List<Points> getByCompany(int id);
	public Points getByUserAndCompany(int user_id, int company_id);

	public int getCompanyByCode(String code);
	public int getValueByCode(String code);

}
