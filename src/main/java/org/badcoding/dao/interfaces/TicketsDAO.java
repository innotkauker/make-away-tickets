package org.badcoding.dao.interfaces;

import java.util.List;

import org.badcoding.hibernate.stored.Flights;
import org.badcoding.hibernate.stored.Tickets;
import org.badcoding.hibernate.stored.Users;

public interface TicketsDAO {
	public void add(Tickets ticket);
	public void update(Tickets ticket);
	public void delete(Tickets ticket);
	public Tickets getById(int id);
	
	public List<Tickets> getByUser(Users user, Boolean paid);
	public List<Tickets> getByFlight(Flights flight, Integer type);
	public List<Tickets> getByFlight(Flights flight);
}
