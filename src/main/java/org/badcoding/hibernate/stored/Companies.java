package org.badcoding.hibernate.stored;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name = "companies")
public class Companies {
	@Id
	@Column(name = "company_id")
	private int company_id;
	@OneToMany(mappedBy="pk.company")
	private Set<Points> points = new HashSet<Points>();
	@Column(name = "title")
	private String title;
	@Column(name = "logo_url")
	private String logo_url;
	@Column(name = "bonus_points_ratio")
	private float bonus_points_ratio;
	@Column(name = "bonus_points_activated")
	private float bonus_points_activated;
	@Column(name = "sold_tickets_value")
	private float sold_tickets_value;

	public Companies() {}

	public Set<Points> getPoints() {
		return points;
	}
	public void setPoints(Set<Points> points) {
		this.points = points;
	}
	public int getCompany_id() {
		return company_id;
	}
	public void setCompany_id(int company_id) {
		this.company_id = company_id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getLogo_url() {
		return logo_url;
	}
	public void setLogo_url(String logo_url) {
		this.logo_url = logo_url;
	}
	public float getBonus_points_ratio() {
		return bonus_points_ratio;
	}
	public void setBonus_points_ratio(float bonus_points_ratio) {
		this.bonus_points_ratio = bonus_points_ratio;
	}
	public float getBonus_points_activated() {
		return bonus_points_activated;
	}
	public void setBonus_points_activated(float bonus_points_activated) {
		this.bonus_points_activated = bonus_points_activated;
	}
	public float getSold_tickets_value() {
		return sold_tickets_value;
	}
	public void setSold_tickets_value(float sold_tickets_value) {
		this.sold_tickets_value = sold_tickets_value;
	}
}

