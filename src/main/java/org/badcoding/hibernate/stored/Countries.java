package org.badcoding.hibernate.stored;
import javax.persistence.*;

@Entity
@Table(name = "countries")
public class Countries {
	@Id
	@Column(name = "country_id")
	private int country_id;
	@Column(name = "name")
	private String name;
	@Column(name = "flag_url")
	private String flag_url;

	public Countries() {}
	public Countries(int country_id, String name, String flag_url) {
		this.country_id = country_id;
		this.name = name;
		this.flag_url = flag_url;
	}
	public int getCountry_id() {
		return country_id;
	}
	public void setCountry_id(int country_id) {
		this.country_id = country_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getFlag_url() {
		return flag_url;
	}
	public void setFlag_url(String flag_url) {
		this.flag_url = flag_url;
	}
}

