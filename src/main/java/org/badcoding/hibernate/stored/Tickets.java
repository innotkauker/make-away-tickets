package org.badcoding.hibernate.stored;
import javax.persistence.*;

@Entity
@Table(name = "tickets", uniqueConstraints = {
			@UniqueConstraint(columnNames={"flight_id", "seat"})
		})
public class Tickets {
	@Id
	private int ticket_id;
	@ManyToOne
	@JoinColumn(name = "user_id")
	private Users user;
	@ManyToOne
	@JoinColumn(name = "flight_id")
	private Flights flight;
	@Column(name = "seat")
	private int seat;
	@Column(name = "type")
	private int type;
	@Column(name = "cost")
	private double cost;
	@Column(name = "paid")
	private double paid;
	@Column(name = "payment_secret")
	private String payment_secret;
	@Column(name = "destination_wallet")
	private String destination_wallet;

	public Tickets() {}
	public int getTicket_id() {
		return ticket_id;
	}
	public void setTicket_id(int ticket_id) {
		this.ticket_id = ticket_id;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
	public Flights getFlight() {
		return flight;
	}
	public void setFlight(Flights flight) {
		this.flight = flight;
	}
	public int getSeat() {
		return seat;
	}
	public void setSeat(int seat) {
		this.seat = seat;
	}
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getPaid() {
		return paid;
	}
	public void setPaid(double paid) {
		this.paid = paid;
	}
	public String getPayment_secret() {
		return payment_secret;
	}
	public void setPayment_secret(String payment_secret) {
		this.payment_secret = payment_secret;
	}
	public String getDestination_wallet() {
		return destination_wallet;
	}
	public void setDestination_wallet(String destination_wallet) {
		this.destination_wallet = destination_wallet;
	}
}

