package org.badcoding.hibernate.stored;

import java.io.Serializable;
import javax.persistence.*;

@Embeddable
public class UserCompany implements Serializable{
	@ManyToOne
	private Users user;
	@ManyToOne
	private Companies company;
	
	public UserCompany() {}
	public Companies getCompany() {
		return company;
	}
	public void setCompany(Companies company) {
		this.company = company;
	}
	public Users getUser() {
		return user;
	}
	public void setUser(Users user) {
		this.user = user;
	}
}
