package org.badcoding.hibernate.stored;
import java.util.HashSet;
import java.util.Set;
import org.mindrot.jbcrypt.*;

import javax.persistence.*;

@Entity
@Table(name = "users")
public class Users {
	@Id
	@Column(name = "user_id")
	@GeneratedValue
	private int user_id;
	@OneToMany(mappedBy="pk.user", fetch = FetchType.LAZY)
	private Set<Points> points = new HashSet<Points>();
	@Column(name = "email")
	private String email;
	@Column(name = "name")
	private String name;
	@Column(name = "last_name")
	private String last_name;
	@Column(name = "patronymic")
	private String patronymic;
	@Column(name = "password_hash")
	private String password_hash;
	@Column(name = "last_login")
	private java.util.Date last_login;
	@Column(name = "date_registered")
	private java.util.Date date_registered;

	public Users() {}

	public boolean checkPassword(String candidate) {
		if (BCrypt.checkpw(candidate, password_hash))
			return true;
		else
			return false;
	}

	public void setPassword(String password) {
		String hashed = BCrypt.hashpw(password, BCrypt.gensalt());
		password_hash = hashed;
	}

	public Set<Points> getPoints() {
		return points;
	}
	public void setTickets(Set<Points> points) {
		this.points = points;
	}
	public int getUser_id() {
		return user_id;
	}
	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getPatronymic() {
		return patronymic;
	}
	public void setPatronymic(String patronymic) {
		this.patronymic = patronymic;
	}
	public String getPassword_hash() {
		return password_hash;
	}
	public void setPassword_hash(String password_hash) {
		this.password_hash = password_hash;
	}
	public java.util.Date getLast_login() {
		return last_login;
	}
	public void setLast_login(java.util.Date last_login) {
		this.last_login = last_login;
	}
	public java.util.Date getDate_registered() {
		return date_registered;
	}
	public void setDate_registered(java.util.Date date_registered) {
		this.date_registered = date_registered;
	}
}

