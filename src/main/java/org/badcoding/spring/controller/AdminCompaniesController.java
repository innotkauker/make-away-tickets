package org.badcoding.spring.controller;

import java.util.*;
import javax.servlet.http.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;
import org.badcoding.dao.interfaces.*;
import org.badcoding.hibernate.stored.*;
import org.badcoding.spring.form.*;

@Controller
@RequestMapping("/admin")
public class AdminCompaniesController {
	@Autowired
	UsersDAO users;

	@Autowired
	TicketsDAO tickets;

	@Autowired
	FlightsDAO flights;

	@Autowired
	CompaniesDAO companies;

	@Autowired
	PointsDAO points;

	@Autowired
	CitiesDAO cities;

	private Boolean is_admin(HttpServletRequest request) {
		HttpSession session = request.getSession();
		return (session.getAttribute("is_admin") != null);
	}

	@RequestMapping("/companies")
	public String companies(HttpServletRequest request, Map<String, Object> model) {
		List<Integer> errors = new ArrayList<Integer>();
		if (!is_admin(request)) {
			errors.add(45);
			model.put("e", errors);
			return "redirect:/index";
		}
		model.put("companiesForm", new CompaniesForm());
		return "/admin/companies";
	}

	@RequestMapping(value = "/companies_search", method = RequestMethod.GET)
	public String companies_search(@ModelAttribute CompaniesForm companiesForm, HttpServletRequest request, Map<String, Object> model) {
		List<Integer> errors = new ArrayList<Integer>();
		List<Companies> result = new ArrayList<Companies>();
		if (!is_admin(request)) {
			errors.add(45);
			model.put("e", errors);
			return "redirect:/index";
		}
		try {
			String id_s = companiesForm.getId();
			String title = companiesForm.getTitle();
			String activated_bonus_gt_s = companiesForm.getActivated_bonus_gt();
			String sold_tickets_gt_s = companiesForm.getSold_tickets_gt();
			String flights_s = companiesForm.getFlights();
			if (!id_s.equals("") && (!title.equals("") || !activated_bonus_gt_s.equals("") || !sold_tickets_gt_s.equals("") || !flights_s.equals(""))) {
				errors.add(46);
				throw new Exception();
			}
			if (id_s == "")
				id_s = "-1";
			if (activated_bonus_gt_s == "")
				activated_bonus_gt_s = "-1";
			if (sold_tickets_gt_s == "")
				sold_tickets_gt_s = "-1";
			if (flights_s == "")
				flights_s = "-1";

			Integer id = null;
			Integer activated_bonus_gt = null;
			Integer sold_tickets_gt = null;
			Integer flights = null;
			try {
				id = Integer.parseInt(id_s);
				activated_bonus_gt = Integer.parseInt(activated_bonus_gt_s);
				sold_tickets_gt = Integer.parseInt(sold_tickets_gt_s);
				flights = Integer.parseInt(flights_s);
			} catch (Exception e) {
				errors.add(47);
				throw e;
			}
			if (id != -1) {
				Companies t = companies.getById(id);
				if (t != null)	
					result.add(t);
			} else {
				result = companies.getByFilters(title, activated_bonus_gt, sold_tickets_gt, flights);
			}
			if (result.size() == 0) {
				errors.add(48);
				model.put("info", errors);
			} else {
				model.put("results", result);
			}
		} catch (Exception e) {
			model.put("errors", errors);
		}
		model.put("companiesForm", companiesForm);
		return "/admin/companies";
	}

	@RequestMapping(value="/add_company", method=RequestMethod.POST)
	public @ResponseBody List<Integer> add_company(HttpServletRequest request, CompaniesEditForm companiesEditForm) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			String title = companiesEditForm.getTitle();
			String url = companiesEditForm.getUrl();
			String bpr_s = companiesEditForm.getBpr();
			String bpa_s = companiesEditForm.getBpa();
			String stv_s = companiesEditForm.getStv();
			if (bpr_s == "")
				bpr_s = "0";
			if (bpa_s == "")
				bpa_s = "0";
			if (stv_s == "")
				stv_s = "0";
			if (title.equals("")) {
				errors.add(49);
				throw new Exception();
			}
			// Integer id = null;
			Float bpr = null;
			Float bpa = null;
			Float stv = null;
			try {
				// id = Integer.parseInt(id_s);
				bpr = Float.parseFloat(bpr_s);
				bpa = Float.parseFloat(bpa_s);
				stv = Float.parseFloat(stv_s);
			} catch (Exception e) {
				errors.add(47);
				throw e;
			}
			Companies c = new Companies();
			c.setTitle(title);
			c.setLogo_url(url);
			c.setBonus_points_ratio(bpr);
			c.setBonus_points_activated(bpa);
			c.setSold_tickets_value(stv);
			companies.add(c);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value="/get_company", method=RequestMethod.GET)
	public @ResponseBody List<List<String>> get_company(HttpServletRequest request, @RequestParam int company_id) {
		List<String> errors = new ArrayList<String>();
		List<List<String>> result = new ArrayList<List<String>>();
		try {
			if (!is_admin(request)) {
				errors.add("45");
				throw new Exception();
			}
			Companies c = companies.getById(company_id);
			if (c == null) {
				errors.add("48");
				throw new Exception();
			}
			List<String> t = new ArrayList<String>();
			t.add(Integer.toString(c.getCompany_id()));
			t.add(c.getTitle());
			t.add(c.getLogo_url());
			t.add(Float.toString(c.getBonus_points_ratio()));
			t.add(Float.toString(c.getBonus_points_activated()));
			t.add(Float.toString(c.getSold_tickets_value()));
			result.add(t);
			result.add(errors);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add("43");
			result.add(errors);
		}
		return result;
	}

	@RequestMapping(value="/remove_company", method=RequestMethod.POST)
	public @ResponseBody List<Integer> remove_company(HttpServletRequest request, Integer company_id) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			Companies c = companies.getById(company_id);
			if (c == null) {
				errors.add(50);
				throw new Exception();
			}
			companies.delete(c);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

	@RequestMapping(value="/edit_company", method=RequestMethod.POST)
	public @ResponseBody List<Integer> edit_company(HttpServletRequest request, CompaniesEditForm companiesEditForm) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			if (!is_admin(request)) {
				errors.add(45);
				throw new Exception();
			}
			String id_s = companiesEditForm.getId();
			String title = companiesEditForm.getTitle();
			String url = companiesEditForm.getUrl();
			String bpr_s = companiesEditForm.getBpr();
			String bpa_s = companiesEditForm.getBpa();
			String stv_s = companiesEditForm.getStv();

			Integer id = null;
			Float bpr = null;
			Float bpa = null;
			Float stv = null;
			try {
				id = Integer.parseInt(id_s);
			} catch (Exception e) {
				errors.add(47);
				throw e;
			}
			Companies c = companies.getById(id);
			if (c == null) {
				errors.add(50);
				throw new Exception();
			}
			if (title != "") 
				c.setTitle(title);
			if (url != "") 
				c.setLogo_url(url);
			if (bpr_s != "")
				try {
					bpr = Float.parseFloat(bpr_s);
					c.setBonus_points_ratio(bpr);
				} catch (Exception e) {
					errors.add(47);
					throw e;
				}
			if (bpa_s == "")
				try {
					bpa = Float.parseFloat(bpa_s);
					c.setBonus_points_activated(bpa);
				} catch (Exception e) {
					errors.add(47);
					throw e;
				}
			if (stv_s == "")
				try {
					stv = Float.parseFloat(stv_s);
					c.setSold_tickets_value(stv);
				} catch (Exception e) {
					errors.add(47);
					throw e;
				}
			companies.update(c);
		} catch (Exception e) {
			if (errors.size() == 0)
				errors.add(43);
		}
		return errors;
	}

}
