package org.badcoding.spring.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;
import org.badcoding.dao.interfaces.*;
import org.badcoding.hibernate.stored.*;

@Controller
public class BlockchainController {
	@Autowired
	TicketsDAO tickets;

	@Autowired
	BlockchainAPI api;

	@RequestMapping("/payment_received")
	public @ResponseBody String receive_payment(@RequestParam int ticket_id, @RequestParam Long value, @RequestParam String destination_address, @RequestParam String secret) {
		if (!destination_address.equals(api.getAddress()))
			return "error";
		Tickets t = tickets.getById(ticket_id);
		if (t == null)
			return "error";
		if (!t.getPayment_secret().equals(secret))
			return "error";
		t.setPaid(t.getPaid() + value/100000000.0);
		tickets.update(t);
        return "*ok*";
	}
}
