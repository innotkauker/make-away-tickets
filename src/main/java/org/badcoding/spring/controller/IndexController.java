package org.badcoding.spring.controller;

import java.util.*;
import java.util.regex.*;
import java.text.*;
import javax.servlet.http.*;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.*;
import org.badcoding.dao.interfaces.*;
import org.badcoding.hibernate.stored.*;
import org.badcoding.Application;
import org.badcoding.spring.form.*;
import org.springframework.validation.BindingResult;

@Controller
public class IndexController {
	@Autowired
	UsersDAO users;

	@Autowired
	TicketsDAO tickets;

	@Autowired
	FlightsDAO flights;

	@Autowired
	CompaniesDAO companies;

	@Autowired
	PointsDAO points;

	@Autowired
	CitiesDAO cities;

	@Value("${application.admin}")
    private String admin;

	@Value("${application.admin.password}")
    private String admin_password;

	@RequestMapping("/")
	public String root(Map<String, Object> model) {
        return "redirect:/index";
	}

	@RequestMapping("/set_locale")
	public String set_locale(HttpServletRequest request, HttpServletResponse response, @RequestParam(value="locale", required=false) String locale) {
		Application.localeResolver().setLocale(request, response, new Locale(locale));
        return "redirect:/index";
	}

	@RequestMapping("/index")
	public String index(@ModelAttribute IndexForm indexForm, @RequestParam(value="e", required=false) List<Integer> errors, Map<String, Object> model) {
		model.put("indexForm", (indexForm == null ? new IndexForm() : indexForm));
		if (errors != null)
			model.put("errors", errors);
		return "index";
	}

	@RequestMapping(value="/get_tickets", method=RequestMethod.GET)
	public @ResponseBody List<List<String>> get_tickets(@RequestParam int flight_id, @RequestParam int type) {
		List<Tickets> t = tickets.getByFlight(flights.getById(flight_id), (type == -1 ? null : type));
		// System.out.println("result size = " + t.size());
		List<List<String>> result = new ArrayList<List<String>>(t.size());
		for (int i = 0; i < t.size(); i++) {
			List<String> ticket = new ArrayList<String>(3);
			ticket.add(String.valueOf(t.get(i).getSeat()));
			ticket.add(String.valueOf(t.get(i).getType()));
			ticket.add(String.valueOf(t.get(i).getCost()));
			ticket.add(String.valueOf(t.get(i).getTicket_id()));
			result.add(ticket);
		}
		return result;
	}

	@RequestMapping("/login")
	public String login(@RequestParam(value="info", required=false) List<Integer> info, @RequestParam(value="ticket_id", required=false) Integer ticket_id, Map<String, Object> model) {
		model.put("registrationForm", new RegistrationForm());
		model.put("loginForm", new LoginForm());
		if (ticket_id != null)
			model.put("ticket_id", ticket_id);
		if (info != null)
			model.put("info", info);
		return "login";
	}

	@RequestMapping(value = "/process_login", method = RequestMethod.POST)
	public String process_login(HttpServletRequest request, @RequestParam(value="ticket_id", required=false) Integer ticket_id, @ModelAttribute("loginForm") LoginForm loginForm, Map<String, Object> model, BindingResult result) {
		List<Integer> errors = new ArrayList<Integer>();
		try {
			String email = loginForm.getEmail();
			String password = loginForm.getPassword();

			if (email.equals(admin)) { // admin login
				if (!password.equals(admin_password)) {
					errors.add(32);
					throw new Exception();
				}
				HttpSession session = request.getSession();
				session.setAttribute("is_admin", true);
				return "redirect:/admin/";
			}

			List<Users> user_a = users.getByEmail(email);
			if (user_a == null || user_a.size() == 0) {
				errors.add(33);
				throw new Exception();
			}
			Users user = user_a.get(0);
			if (!user.checkPassword(password)) {
				errors.add(32);
				throw new Exception();
			}

			HttpSession session = request.getSession();
			session.setAttribute("user_id", user.getUser_id());
			if (ticket_id != null) {
				model.put("ticket_id", ticket_id);
				return "redirect:/user/order_ticket";
			}
			return "redirect:/user/";
		} catch (Exception e) {
			model.put("loginForm", loginForm);
			model.put("registrationForm", new RegistrationForm());
			model.put("login_errors", errors);
			if (ticket_id != null) {
				model.put("ticket_id", ticket_id);
			}
	    	return "login";
		}

	}

	@RequestMapping(value = "/process_registration", method = RequestMethod.POST)
	public String register(@ModelAttribute("registrationForm") RegistrationForm registrationForm, @RequestParam(value="ticket_id", required=false) Integer ticket_id, Map<String, Object> model, BindingResult result) {
		String email = registrationForm.getEmail();
		String name = registrationForm.getName();
		String last_name = registrationForm.getLast_name();
		String patronymic = registrationForm.getPatronymic();
		String password = registrationForm.getPassword();
		String re_password = registrationForm.getRe_password();
		List<Integer> errors = new ArrayList<Integer>();
		Pattern email_regex = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$", Pattern.CASE_INSENSITIVE);

		if (!password.equals(re_password))
			errors.add(20);
		if (password.length() < 8)
			errors.add(21);
		if (name.length() < 4)
			errors.add(22);
		if (last_name.length() < 4)
			errors.add(23);
		if (!email_regex.matcher(email).matches())
			errors.add(24);
		if (users.getByEmail(email).size() != 0)
			errors.add(25);

		if (errors.size() == 0) {
			Users user = new Users();
			user.setEmail(email);
			user.setName(name);
			user.setLast_name(last_name);
			user.setPatronymic(patronymic);
			user.setPassword(password);
			user.setDate_registered(new Date());
			user.setLast_login(null);
			users.add(user);
			// user.checkPassword(password);
			model.put("registration_successful", true);
			model.put("registrationForm", new RegistrationForm());
			model.put("loginForm", new LoginForm());
			if (ticket_id != null) {
				model.put("ticket_id", ticket_id);
			}
			return "login";
		}
		model.put("registrationForm", registrationForm);
		model.put("loginForm", new LoginForm());
		model.put("errors", errors);
		if (ticket_id != null) {
			model.put("ticket_id", ticket_id);
		}
	    return "login";
	}

	@RequestMapping(value = "/index_search", method = RequestMethod.GET)
	public String list_flights(@ModelAttribute IndexForm indexForm, Map<String, Object> model) {
		String city_out = indexForm.getCity_out();
		String city_in = indexForm.getCity_in();
		String company = indexForm.getCompany();
		String date0 = indexForm.getDate0();
		String date1 = indexForm.getDate1();
	
		List<Integer> errors = new ArrayList<Integer>();
		Date start = null;
		if (date0 != "")
			try {
				start = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).parse(date0);
			} catch (Exception e) {
				try {
					start = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH).parse(date0);
				} catch (Exception r) {
					errors.add(10);
				}
			}

		Date end = null;
		if (date1 != "")
			try {
				end = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH).parse(date1);
			} catch (Exception e) {
			   	try {
					end = new SimpleDateFormat("dd.MM.yyyy HH:mm", Locale.ENGLISH).parse(date1);
				} catch (Exception r) {
					errors.add(11);
				}
			}

		List<Cities> city0 = (city_out == "" ? new ArrayList<Cities>() : cities.getByTitle(city_out));
		if (city_out == "")
			city0.add(null);
		if (city0.size() == 0)
			errors.add(12);
		else if (city0.size() > 1)
			errors.add(13);

		List<Cities> city1 = (city_in == "" ? new ArrayList<Cities>() : cities.getByTitle(city_in));
		if (city_in == "")
			city1.add(null);
		if (city1.size() == 0)
			errors.add(14);
		else if (city1.size() > 1)
			errors.add(15);

		List<Companies> comp = (company == "" ? new ArrayList<Companies>() : companies.getByFilters(company, -1, -1, -1));
		if (company == "")
			comp.add(null);
		if (comp.size() == 0)
			errors.add(16);
		else if (comp.size() > 1)
			errors.add(17);

		List<Flights> result = null;
		if (errors.size() == 0) {
			result = flights.getByChoice(start, end, city0.get(0), city1.get(0), comp.get(0), true);
			if (result.size() == 0)
				errors.add(18);
		}

		if (errors.size() == 0) {
			model.put("results", result);
	    	return "index_search";
		} else {
			model.put("errors", errors);
			model.put("indexForm", indexForm);
			return "index";
		}
	}
}
