package org.badcoding.spring.form;

public class CompaniesEditForm {
	private String id;
	private String title;
	private String url;
	private String bpr;
	private String bpa;
	private String stv;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getBpr() {
		return bpr;
	}
	public void setBpr(String bpr) {
		this.bpr = bpr;
	}
	public String getBpa() {
		return bpa;
	}
	public void setBpa(String bpa) {
		this.bpa = bpa;
	}
	public String getStv() {
		return stv;
	}
	public void setStv(String stv) {
		this.stv = stv;
	}
}


