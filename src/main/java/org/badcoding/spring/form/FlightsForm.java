package org.badcoding.spring.form;

public class FlightsForm {
	private String id;
	private String start;
	private String end;
	private String city_out;
	private String city_in;
	private String company;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getStart() {
		return start;
	}
	public void setStart(String start) {
		this.start = start;
	}
	public String getEnd() {
		return end;
	}
	public void setEnd(String end) {
		this.end = end;
	}
	public String getCity_out() {
		return city_out;
	}
	public void setCity_out(String city_out) {
		this.city_out = city_out;
	}
	public String getCity_in() {
		return city_in;
	}
	public void setCity_in(String city_in) {
		this.city_in = city_in;
	}
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
}
