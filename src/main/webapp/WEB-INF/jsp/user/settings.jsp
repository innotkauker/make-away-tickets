<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><spring:message code="title.settings" /></title>

    <!-- Bootstrap core CSS -->
    <link href="/js/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	<%@include file="../include/navbar.jsp" %>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<li><a href="<c:url value="/user/tickets" />"><spring:message code="label.user.01" /></a></li>
					<li><a href="<c:url value="/user/points" />"><spring:message code="label.user.02" /></a></li>
					<li class="active"><a href="<c:url value="/user/settings" />"><spring:message code="label.user.03" /></a></li>
				</ul>
				<ul class="nav nav-sidebar">
					<li><a href="<c:url value="/user/logout" />"><spring:message code="label.user.12" /></a></li>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header"><spring:message code="label.user.03" /></h1>
				<div class="row">
        			<div class="col-md-5 col-md-offset-1 main">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3><spring:message code="label.user.04" /></h3>
							</div>
							<div class="panel-body">

        			  			<form:form id="edit_form" onsubmit='return false;' commandName="settingsForm" role="form">
									<spring:message code="label.login.1" var="msg1" />
									<spring:message code="label.login.2" var="msg2" />
									<spring:message code="label.login.3" var="msg3" />
									<spring:message code="label.login.4" var="msg4" />
									<spring:message code="label.login.5" var="msg5" />
									<spring:message code="label.login.6" var="msg6" />
									<spring:message code="label.user.05" var="msg05" />
									<p><form:input id="email" path="email" name="email" type="text" class="form-control" placeholder="${ msg1 }" /></p>
									<p><form:input id="name" path="name" name="name" type="text" class="form-control" placeholder="${ msg4 }" /></p>
									<p><form:input id="last_name" path="last_name" name="last_name" type="text" class="form-control" placeholder="${ msg5 }" /></p>
									<p><form:input id="patronymic" path="patronymic" name="patronymic" type="text" class="form-control" placeholder="${ msg6 }" /></p>
									<p><form:input id="password" path="password" name="password" type="password" class="form-control" placeholder="${ msg2 }" /></p>
									<p><form:input id="re_password" path="re_password" name="re_password" type="password" class="form-control" placeholder="${ msg3 }" /></p>
									<p><input id="old_password" name="old_password" type="password" class="form-control" placeholder="${ msg05 }" /></p>
									<p><button id="edit_button" class="btn btn-primary"><spring:message code="label.user.06" /></button></p>
        			  			</form:form>
							</div>
						</div>
						<div id="errors_div" class="col-xs-10 col-xs-offset-1"></div>

        			</div>
        			<div class="col-md-5 col-md-offset-0 main">
						<div class="panel panel-default">
							<div class="panel-heading">
								<h3><spring:message code="label.user.07" /></h3>
							</div>
							<div class="panel-body">

        			  			<form id="points_form" onsubmit='return false;' role="form">
									<spring:message code="label.user.08" var="msg08" />
									<p><input id="code" name="code" type="text" class="form-control" placeholder="${ msg08 }" /></p>
									<p><button id="submit_button" class="btn btn-primary"><spring:message code="label.user.09" /></button></p>
        			  			</form>
							</div>
						</div>
						<div id="code_errors_div" class="col-xs-10 col-xs-offset-1"></div>

        			</div>
				</div>
			</div>

		</div>
	</div>
    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/js/bootstrap.min.js"></script>
	<script>
		var btn = $("#edit_button")[0]
		btn.disabled = false;
		btn.onclick = function() {
			$('#errors_p').remove()
			var btn = $(this)
			btn.button('loading')
			$.post( '<c:url value='/user/edit_user' />'
				, $('#edit_form').serialize()
				, function(data) {
					$('<p/>', {
						id: 'errors_p'
					}).appendTo('#errors_div');
					if (data.length == 0) {
						$('<div/>', {
						    class: 'alert alert-success fade in',
							id: 'alert_success'
						}).appendTo('#errors_p');
						$('#alert_success').append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
						window['msg1000'].appendTo('#alert_success')
					} else {
						var i;
						for (i = 0; i < data.length; ++i) {
							var error = data[i]
							$('<div/>', {
							    class: 'alert alert-danger fade in',
								id: 'alert_error' + i.toString()
							}).appendTo('#errors_p');
							$('#alert_error' + i.toString()).append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
							window['msg' + error.toString()].appendTo('#alert_error' + i.toString())
						};
					}
					btn.button('reset')
				});
			}
	</script>
	<script>
		var btn = $("#submit_button")[0]
		btn.disabled = false;
		btn.onclick = function() {
			$('#code_errors_p').remove()
			var btn = $(this)
			btn.button('loading')
			$.post( '<c:url value='/user/add_points' />'
				, { code: $('#code').val() }
				, function(data) {
					$('<p/>', {
						id: 'code_errors_p'
					}).appendTo('#code_errors_div');
					if (data.length == 0) {
						$('<div/>', {
						    class: 'alert alert-success fade in',
							id: 'code_alert_success'
						}).appendTo('#code_errors_p');
						$('#code_alert_success').append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
						window['msg1000'].appendTo('#code_alert_success')
					} else {
						var i;
						for (i = 0; i < data.length; ++i) {
							var error = data[i]
							$('<div/>', {
							    class: 'alert alert-danger fade in',
								id: 'code_alert_error' + i.toString()
							}).appendTo('#code_errors_p');
							$('#code_alert_error' + i.toString()).append('<button class="close" aria-hidden="true" data-dismiss="alert" type="button">×</button>')
							window['msg' + error.toString()].appendTo('#code_alert_error' + i.toString())
						};
					}
					btn.button('reset')
				});
			}
	</script>
	<%@include file="../include/errors.jsp" %>
  </body>
</html>
