<%@ page language="java" import="org.badcoding.hibernate.stored.*,java.text.*" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

	<title><spring:message code="title.tickets" /></title>

    <!-- Bootstrap core CSS -->
    <link href="/js/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/dashboard.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body>
	<%@include file="../include/navbar.jsp" %>

	<!-- ticket detail modal -->
	<div id="tickets-info-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="infoModalLabel" aria-hidden="true">
	  <div class="modal-dialog modal-lg">
	    <div id="info-modal-div" class="modal-content" align="center">
			<div class="modal-header" align="left">
				<button class="close" aria-hidden="true" data-dismiss="modal" type="button">
					×
				</button>
				<h4 id="infoModalLabel" class="modal-title">
					<spring:message code="label.user.14" />
				</h4>
			</div>
			<p id="tickets-info-p">
				<table class="table">
   					<tbody>
   						<tr>
							<td><spring:message code="label.index.5" /></td>
							<td><spring:message code="label.index.6" /></td>
							<td><spring:message code="label.index.7" /></td>
							<td><b><spring:message code="label.index.20" />:</b> <span id="seat"></span></td>
   						</tr>
   						<tr>
							<td><b><spring:message code="label.index.9" />:</b> <span id="city0"></span></td>
							<td><b><spring:message code="label.index.10" />:</b> <span id="flight_id"></span></td>
							<td><b><spring:message code="label.index.9" />:</b> <span id="city1"></span></td>
							<td><b><spring:message code="label.index.21" />:</b> <span id="type"></span></td>
   						</tr>
   						<tr>
							<td><b><spring:message code="label.index.11" />:</b> (<span id="airport0"></span>)</td>
							<td><b><spring:message code="label.index.14" />:</b> <span id="plane"></span></td>
							<td><b><spring:message code="label.index.11" />:</b> (<span id="airport1"></span>)</td>
							<td><b><spring:message code="label.index.22" />:</b> <span id="cost"></span> <spring:message code="label.btc" /></td>
   						</tr>
   						<tr>
							<td><b><spring:message code="label.index.12" />:</b> <span id="date"></span></td>
   							<td><b><spring:message code="label.index.2" />:</b> <span id="company"></span></td>
							<td><b><spring:message code="label.index.13" />:</b> <span id="length"></span><spring:message code="label.km" /></td>
							<td><b><spring:message code="label.user.15" />:</b> <span id="paid"></span> <spring:message code="label.btc" /></td>
   						</tr>
						<tr>
							<td><b><spring:message code="label.user.02" />:</b> <span id="points_total"></span></td>
							<td><b><spring:message code="label.user.17" />:</b> <span id="points_req"></span></td>
							<td colspan="2"align="right">
								<form class="form-horizontal" onsubmit='return false;' role="form">
									<div class="col-sm-7 form-group has-feedback" id="points-input-div">
										<input type="text" class="form-control" id="points-prov" placeholder="<spring:message code="label.user.18" />">
										<span id="ticket-id-div" hidden="true"></span>
									</div>
									<div class="col-sm-3">
										<button type="submit" id="add-points-btn" class="btn btn-default"><spring:message code="label.user.19" /></button>
									</div>
								</form>
							</td>
						</tr>
						<tr>
							<td align="right"><b><spring:message code="label.user.16" />:</b></td>
							<td colspan="3"><span id="address"></span></td>
						</tr>
   					</tbody>
				</table>
			</p>
	    </div>
	  </div>
	</div>

	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-3 col-md-2 sidebar">
				<ul class="nav nav-sidebar">
					<li class="active"><a href="<c:url value="/user/tickets" />"><spring:message code="label.user.01" /></a></li>
					<li><a href="<c:url value="/user/points" />"><spring:message code="label.user.02" /></a></li>
					<li><a href="<c:url value="/user/settings" />"><spring:message code="label.user.03" /></a></li>
				</ul>
				<ul class="nav nav-sidebar">
					<li><a href="<c:url value="/user/logout" />"><spring:message code="label.user.12" /></a></li>
				</ul>
			</div>
			<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
				<h1 class="page-header"><spring:message code="label.user.01" /></h1>
				<c:if test="${ !empty info }">
					<p>
					<div class="col-xs-10 col-xs-offset-1">
						<c:forEach items="${ info }" var="error">
							<div class="alert alert-info fade in">
								<button class="close" aria-hidden="true" data-dismiss="alert" type="button">
									×
								</button>
								<spring:message code="error.${ error }" />
							</div>
						</c:forEach>
					</div>
					</p>
				</c:if>
				<div class="row">
					<div class="col-md-8 col-md-offset-2">
						<c:forEach items="${ results }" var="result">
							<div class="highlight">
								<table class="table" style="width: 100%">
									<tbody>
										<tr>
											<td><b><spring:message code="label.index.10" />:</b> ${ result.flight.flight_id }</td>
											<td><b><spring:message code="label.index.5" />:</b> ${ result.flight.airport_out.city.name }</td>
											<td><b><spring:message code="label.index.7" />:</b> ${ result.flight.airport_in.city.name }</td>
										</tr>
										<tr>
											<td><%= new SimpleDateFormat("dd.MM.yyyy").format(((Tickets)(pageContext.findAttribute("result"))).getFlight().getDate()) %></td>
											<td><b><spring:message code="label.index.20" />:</b> ${ result.seat }</td>
											<td></td>
										</tr>
										<tr>
											<td><%= new SimpleDateFormat("HH:mm").format(((Tickets)(pageContext.findAttribute("result"))).getFlight().getDate()) %></td>
											<td><b><spring:message code="label.user.13" />:</b> <%= ((Tickets)(pageContext.findAttribute("result"))).getCost() - ((Tickets)(pageContext.findAttribute("result"))).getPaid() %> <spring:message code="label.btc" /></td>
											<td align="right">
												<button type="button" class="btn btn-xs btn-primary" id="info_btn_${ result.ticket_id }">
													<span class="glyphicon glyphicon-zoom-in"></span>
													<spring:message code="label.user.14" />
												</button>
											</td>
										</tr>
									</tbody>
								</table>
							</div>
						</c:forEach>
					</div>
				</div>
			</div>

		</div>
	</div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
    <script src="/js/js/bootstrap.min.js"></script>
	<script>
		$.each($(":button[id^='info_btn_']"), function(i, btn) {
			btn.disabled = false;
			btn.onclick = function() {
				var btn = $(this)
				btn.button('loading')
				$('#points-status-span').remove()
				$('#points-prov').val("")
				var i = btn.attr("id").match(/[\d]+$/)[0]
				var request = $.getJSON("/user/ticket_info", { ticket_id: i }, function(data) {
					for (i = 0; i < data.length; ++i)
						$('#' + data[i][0]).text(data[i][1])
					$('#tickets-info-modal').modal()
					btn.button('reset')
				})
				request.fail(function() {
					$('<div/>', {
						text: "<spring:message code="label.index.19" />",
					}).appendTo('#info-modal-div')
					$('#tickets-info-modal').modal()
					btn.button('reset')
				})
			}
		})
	</script>
	<script>
		var btn = $("#add-points-btn")[0]
		btn.disabled = false;
		btn.onclick = function() {
			$('#points-status-span').remove()
			var btn = $(this)
			btn.button('loading')
			$.post( '<c:url value='/user/pay_with_points' />'
				, { value: $('#points-prov').val(), ticket_id: $('#ticket-id-div').text() }
				, function(data) {
					if (data == "ok") {
						$('#points-input-div').append('<span id="points-status-span" class="glyphicon glyphicon-ok form-control-feedback"></span>')
						$('#points-prov').val("")
					} else {
						$('#points-input-div').append('<span id="points-status-span" class="glyphicon glyphicon-remove form-control-feedback"></span>')
					}
					btn.button('reset')
				});
			}
	</script>
	<c:if test="${ !empty ticket_id }">
		<script>
			$('#info_btn_${ ticket_id }').click()	
		</script>
	</c:if>
	<%@include file="../include/errors.jsp" %>
  </body>
</html>

