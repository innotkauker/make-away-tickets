package org.badcoding.test;

import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AirportsDAOTest extends Assert{

	@Test
	// Tests add() and getById()
	public void add() {
		Cities city = Factory.GetInstance().getCitiesDAO().getById(1); 
		Airports instance = new Airports(6789, "Ax", city);
		Factory.GetInstance().getAirportsDAO().add(instance);
		Airports inserted = Factory.GetInstance().getAirportsDAO().getById(6789);
		assertNotNull(inserted);
		assertEquals(inserted.getAirport_id(), instance.getAirport_id());
	}
	
	@Test(dependsOnMethods = { "add", "update" })
	public void delete() {
		Airports inserted = Factory.GetInstance().getAirportsDAO().getById(6789);
		Factory.GetInstance().getAirportsDAO().delete(inserted);
		Airports removed = Factory.GetInstance().getAirportsDAO().getById(6789);
		assertNull(removed);
	}
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		Airports inserted = Factory.GetInstance().getAirportsDAO().getById(6789);
		inserted.setTitle("test");
		Factory.GetInstance().getAirportsDAO().update(inserted);
		Airports removed = Factory.GetInstance().getAirportsDAO().getById(6789);
		assertEquals("test", removed.getTitle());
	}
}
