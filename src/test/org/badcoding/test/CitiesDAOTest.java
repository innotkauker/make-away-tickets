package org.badcoding.test;
import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;
import java.util.Collection;

public class CitiesDAOTest extends Assert{

	@Test
	public void add() {
		Countries country = Factory.GetInstance().getCountriesDAO().getById(1); 
		Cities instance = new Cities(6789, "Ax", country);
		Factory.GetInstance().getCitiesDAO().add(instance);
		Cities inserted = Factory.GetInstance().getCitiesDAO().getById(6789);
		assertNotNull(inserted);
		assertEquals(inserted.getCity_id(), instance.getCity_id());
	}
	
	@Test(dependsOnMethods = { "getByTitle" })
	public void delete() {
		Cities inserted = Factory.GetInstance().getCitiesDAO().getById(6789);
		Factory.GetInstance().getCitiesDAO().delete(inserted);
		Cities removed = Factory.GetInstance().getCitiesDAO().getById(6789);
		assertNull(removed);
	}
	
	@Test(dependsOnMethods = { "update" })
	public void getByTitle() {
		Collection<Cities> inserted = Factory.GetInstance().getCitiesDAO().getByTitle("test");
		assertNotEquals(inserted.size(), new Integer(0));
		inserted = Factory.GetInstance().getCitiesDAO().getByTitle(null);
		assertNotEquals(inserted.size(), new Integer(0));
	}
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		Cities inserted = Factory.GetInstance().getCitiesDAO().getById(6789);
		inserted.setName("test");
		Factory.GetInstance().getCitiesDAO().update(inserted);
		Cities updated = Factory.GetInstance().getCitiesDAO().getById(6789);
		assertEquals("test", updated.getName());
	}
}
