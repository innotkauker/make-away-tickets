package org.badcoding.test;
import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class CompaniesDAOTest extends Assert{

	@Test
	public void add() {
		Companies instance = Factory.GetInstance().getCompaniesDAO().getById(1);
		assertNotNull(instance);
		instance.setCompany_id(6789);
		Factory.GetInstance().getCompaniesDAO().add(instance);
		Companies inserted = Factory.GetInstance().getCompaniesDAO().getById(6789);
		assertNotNull(inserted);
		assertEquals(inserted.getCompany_id(), instance.getCompany_id());
	}
	
	@Test(dependsOnMethods = { "update" })
	public void delete() {
		Companies inserted = Factory.GetInstance().getCompaniesDAO().getById(6789);
		Factory.GetInstance().getCompaniesDAO().delete(inserted);
		Companies removed = Factory.GetInstance().getCompaniesDAO().getById(6789);
		assertNull(removed);
	}
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		Companies inserted = Factory.GetInstance().getCompaniesDAO().getById(6789);
		inserted.setTitle("test");
		Factory.GetInstance().getCompaniesDAO().update(inserted);
		Companies updated = Factory.GetInstance().getCompaniesDAO().getById(6789);
		assertEquals("test", updated.getTitle());
	}
}

