package org.badcoding.test;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.*;

public class CompaniesFiltersTest extends Assert{
	private List<Integer> init_stats;
	   	
	private List<Integer> get_initial_statistics() {
		List<Integer> result = new ArrayList<Integer>();
		result.add(test0_provider());
		result.add(test1_provider());
		result.add(test2_provider());
		result.add(test3_provider());
		result.add(test4_provider());
		result.add(test5_provider());
		result.add(test6_provider());
		result.add(test7_provider());
		result.add(test8_provider());
		result.add(test9_provider());
		result.add(test10_provider());
		result.add(test11_provider());
		result.add(test12_provider());
		return result;
	}

	private void add_data(int id, int v1, int v2, String title, int flights) throws Exception {
		Companies instance = Factory.GetInstance().getCompaniesDAO().getById(1);
		assertNotNull(instance);
		instance.setCompany_id(id);
		instance.setTitle(title);
		instance.setBonus_points_activated(v1);
		instance.setSold_tickets_value(v2);
		Factory.GetInstance().getCompaniesDAO().add(instance);
		for (int i = 0; i < flights; i++) {
			Flights dependant = Factory.GetInstance().getFlightsDAO().getById(1);
			dependant.setFlight_id(id + i);
			dependant.setCompany(instance);
			SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm"); 
			Date date = null;
			date = sdf.parse("01.05.1980 05:38");
			dependant.setDate(date);
			Factory.GetInstance().getFlightsDAO().add(dependant);
		}
	}

	@BeforeClass
	public void init() throws Exception {
		init_stats = get_initial_statistics();		
		try {
			add_data(6789, 14, 456, "test0", 3);
			add_data(6769, 45, 456, "test1", 6);
		} catch (Exception e) {
			remove_data(6789, 3);
			remove_data(6769, 6);
			throw e;
		} 
	}

	@Test
	public void test0() {
		assertEquals(init_stats.get(0) + 2, test0_provider());
	}

	private int test0_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("", 0, 0, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test1() {
		assertEquals(init_stats.get(1) + 2, test1_provider());
	}

	public int test1_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("test", 0, 0, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test2() {
		assertEquals(init_stats.get(2) + 1, test2_provider());
	}

	public int test2_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("test0", 0, 0, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test3() {
		assertEquals(init_stats.get(3) + 1, test3_provider());
	}

	public int test3_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("test", 19, 0, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test4() {
		assertEquals(init_stats.get(4) + 2, test4_provider());
	}

	public int test4_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters(null, 10, 0, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test5() {
		assertEquals((int)init_stats.get(5), test5_provider());
	}

	public int test5_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("test", 0, 5678, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test6() {
		assertEquals(init_stats.get(6) + 1, test6_provider());
	}

	public int test6_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("test0", 0, 456, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test7() {
		assertEquals(init_stats.get(7) + 1, test7_provider());
	}

	public int test7_provider() {
		List<Companies> result = (List<Companies>)Factory.GetInstance().getCompaniesDAO().getByFilters(null, 0, 0, 4);
		if (result == null)
			return 0;
		for (int i = 0; i < result.size(); i++) 
			System.out.println(result.get(i).getCompany_id());
		return result.size();
	}

	@Test
	public void test8() {
		assertEquals(init_stats.get(8) + 2, test8_provider());
	}

	public int test8_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("te", 0, 12, 1);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test9() {
		assertEquals((int)init_stats.get(9), test9_provider());
	}

	public int test9_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("test", 0, 0, -1);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test10() {
		assertEquals((int)init_stats.get(10), test10_provider());
	}

	public int test10_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("test", -1, 0, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test11() {
		assertEquals((int)init_stats.get(11), test11_provider());
	}

	public int test11_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters(null, 0, -1, 0);
		if (result == null)
			return 0;
		return result.size();
	}

	@Test
	public void test12() {
		assertEquals((int)init_stats.get(12), test12_provider());
	}

	public int test12_provider() {
		Collection<Companies> result = Factory.GetInstance().getCompaniesDAO().getByFilters("fhjh", 0, 0, 2);
		if (result == null)
			return 0;
		return result.size();
	}

	@AfterClass
	public void clean() {
		remove_data(6789, 3);
		remove_data(6769, 6);
	}

	private void remove_data(int id, int flights) {
		for (int i = 0; i < flights; i++) {
			Flights dependant = Factory.GetInstance().getFlightsDAO().getById(id + i);
			if (dependant != null)
				Factory.GetInstance().getFlightsDAO().delete(dependant);
		}	
		Companies inserted = Factory.GetInstance().getCompaniesDAO().getById(id);
		if (inserted != null)
			Factory.GetInstance().getCompaniesDAO().delete(inserted);
	}
}
