package org.badcoding.test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FlightsDAOTest extends Assert {

	@Test
	public void add() throws ParseException {
		Flights instance = Factory.GetInstance().getFlightsDAO().getById(1);
		assertNotNull(instance);
		instance.setFlight_id(6789);
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy hh:mm"); 
		Date date = null;
		date = sdf.parse("01.05.1980 05:38");
		instance.setDate(date);
		
		Factory.GetInstance().getFlightsDAO().add(instance);
		Flights inserted = Factory.GetInstance().getFlightsDAO().getById(6789);
		assertNotNull(inserted);
		assertEquals(inserted.getFlight_id(), instance.getFlight_id());
	}
	
	@Test(dependsOnMethods = { "update" })
	public void delete() {
		Flights inserted = Factory.GetInstance().getFlightsDAO().getById(6789);
		Factory.GetInstance().getFlightsDAO().delete(inserted);
		Flights removed = Factory.GetInstance().getFlightsDAO().getById(6789);
		assertNull(removed);
	}
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		Flights inserted = Factory.GetInstance().getFlightsDAO().getById(6789);
		inserted.setLength(666);
		Factory.GetInstance().getFlightsDAO().update(inserted);
		Flights updated = Factory.GetInstance().getFlightsDAO().getById(6789);
		assertEquals(666, updated.getLength());
	}
}
