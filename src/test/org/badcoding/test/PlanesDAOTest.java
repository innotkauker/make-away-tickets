package org.badcoding.test;
import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PlanesDAOTest extends Assert{

	@Test
	public void add() {
		Planes instance = new Planes(6789, "sdg");
		Factory.GetInstance().getPlanesDAO().add(instance);
		Planes inserted = Factory.GetInstance().getPlanesDAO().getById(6789);
		assertNotNull(inserted);
		assertEquals(inserted.getPlane_id(), instance.getPlane_id());
	}
	
	@Test(dependsOnMethods = { "update" })
	public void delete() {
		Planes inserted = Factory.GetInstance().getPlanesDAO().getById(6789);
		Factory.GetInstance().getPlanesDAO().delete(inserted);
		Planes removed = Factory.GetInstance().getPlanesDAO().getById(6789);
		assertNull(removed);
	}
	
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		Planes inserted = Factory.GetInstance().getPlanesDAO().getById(6789);
		inserted.setTitle("test");
		Factory.GetInstance().getPlanesDAO().update(inserted);
		Planes updated = Factory.GetInstance().getPlanesDAO().getById(6789);
		assertEquals("test", updated.getTitle());
	}
}


