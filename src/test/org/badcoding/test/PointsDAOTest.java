package org.badcoding.test;

import java.util.List;

import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PointsDAOTest extends Assert {
	private Points added;

	@Test
	public void add() {
		List<Points> old = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(1, false);
		Factory.GetInstance().getPointsDAO().add(1, "f7hHg88f47TjGf");
		List<Points> new_list = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(1, false);
		new_list.removeAll(old);
		assertEquals(new_list.size(), 1);
		assertEquals(new_list.get(0).getUser().getUser_id(), 1);
		added = new_list.get(0);
	}
	
	@Test(dependsOnMethods = { "update" })
	public void delete() {
		List<Points> old = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(1, false);
		Factory.GetInstance().getPointsDAO().delete(added);
		List<Points> new_list = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(1, false);
		old.removeAll(new_list);
		assertEquals(old.size(), 1);
		assertEquals(old.get(0).getValue(), 9);
	}
	
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		added.setValue(9);
		List<Points> old = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(1, false);
		Factory.GetInstance().getPointsDAO().update(added);
		List<Points> new_list = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(1, false);
		new_list.removeAll(old);
		assertEquals(new_list.size(), 1);
		assertEquals(new_list.get(0).getValue(), 9);
	}

	@Test
	public void getByCompany() {
		List<Points> old = (List<Points>)Factory.GetInstance().getPointsDAO().getByCompany(1); // because now i am sure this string has company with id=1 coded in it
		Factory.GetInstance().getPointsDAO().add(1, "f7hHg83f47THGf");
		List<Points> new_list = (List<Points>)Factory.GetInstance().getPointsDAO().getByCompany(1);
		new_list.removeAll(old);
		assertEquals(new_list.size(), 1);
		assertEquals(new_list.get(0).getValue(), Factory.GetInstance().getPointsDAO().getValueByCode("f7hHg83f47THGf"));
		Points tmp = new_list.get(0);
		Factory.GetInstance().getPointsDAO().delete(tmp);
	}
	
	@Test
	public void getByUser0() {
		List<Points> old = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(3, false);
		List<Points> old_u = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(3, true);
		Factory.GetInstance().getPointsDAO().add(3, "0Dghtrhj86s"); // this hash sets value to 0
		List<Points> new_list = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(3, false);
		new_list.removeAll(old);
		assertEquals(new_list.size(), 0);
		List<Points> new_list_u = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(3, true);
		new_list_u.removeAll(old_u);
		Points tmp = new_list_u.get(0);
		Factory.GetInstance().getPointsDAO().delete(tmp);
	}
	@Test
	public void getByUser1() {
		List<Points> old = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(3, true);
		Factory.GetInstance().getPointsDAO().add(3, "f7hh0Tk7THGf");
		List<Points> new_list = (List<Points>)Factory.GetInstance().getPointsDAO().getByUser(3, true);
		new_list.removeAll(old);
		assertEquals(new_list.size(), 1);
		Points tmp = new_list.get(0);
		Factory.GetInstance().getPointsDAO().delete(tmp);
	}
	
}
