package org.badcoding.test;

import java.util.List;

import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TicketsDAOTest extends Assert {

	@Test
	public void add() {
		Tickets instance = null;
		try {
			instance = Factory.GetInstance().getTicketsDAO().getById(1);
			assertNotNull(instance);
			instance.setTicket_id(6789);
			Factory.GetInstance().getTicketsDAO().add(instance);
			Tickets inserted = Factory.GetInstance().getTicketsDAO().getById(6789);
			assertNotNull(inserted);
			assertEquals(inserted.getTicket_id(), instance.getTicket_id());
		} catch (Exception e) {
			if (instance != null)
				Factory.GetInstance().getTicketsDAO().delete(instance);
			throw e;
		}
	}
	
	@Test(dependsOnMethods = { "update" })
	public void delete() {
		Tickets inserted = Factory.GetInstance().getTicketsDAO().getById(6789);
		Factory.GetInstance().getTicketsDAO().delete(inserted);
		Tickets removed = Factory.GetInstance().getTicketsDAO().getById(6789);
		assertNull(removed);
	}
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		Tickets inserted = null;
		try {
			inserted = Factory.GetInstance().getTicketsDAO().getById(6789);
			assertNotNull(inserted);
			inserted.setSeat(1984);
			Factory.GetInstance().getTicketsDAO().update(inserted);
			Tickets updated = Factory.GetInstance().getTicketsDAO().getById(6789);
			assertEquals(1984, updated.getSeat());
		} catch (Exception e) {
			if (inserted != null)
				Factory.GetInstance().getTicketsDAO().delete(inserted);
			throw e;
		}
	}

	@Test
	public void getByUser0() {
		Tickets instance0 = null, instance1 = null;
		try {
			Users user = Factory.GetInstance().getUsersDAO().getById(1);
			assertNotNull(user);
			List<Tickets> old = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByUser(user, null);
			instance0 = Factory.GetInstance().getTicketsDAO().getById(1);
			assertNotNull(instance0);
			instance0.setTicket_id(456);
			instance0.setUser(user);
			Factory.GetInstance().getTicketsDAO().add(instance0);
			instance1 = Factory.GetInstance().getTicketsDAO().getById(1);
			instance1.setTicket_id(466);
			instance1.setUser(user);
			Factory.GetInstance().getTicketsDAO().add(instance1);
			List<Tickets> result = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByUser(user, null);
			assertNotNull(result);
			assertEquals(result.size(), old.size() + 2);
		} finally {
			if (instance0 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance0);
			if (instance1 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance1);
		}
	}

	@Test
	public void getByUser1() {
		Tickets instance0 = null, instance1 = null, instance2 = null;
		try {
			Users user = Factory.GetInstance().getUsersDAO().getById(1);
			assertNotNull(user);
			List<Tickets> old = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByUser(user, true);
			List<Tickets> old_n = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByUser(user, false);
			instance0 = Factory.GetInstance().getTicketsDAO().getById(1);
			assertNotNull(instance0);
			instance0.setTicket_id(456);
			instance0.setUser(user);
			instance0.setCost(1.005);
			instance0.setPaid(1.0051);
			Factory.GetInstance().getTicketsDAO().add(instance0);
			instance1 = Factory.GetInstance().getTicketsDAO().getById(1);
			instance1.setTicket_id(866);
			instance1.setUser(user);
			instance1.setCost(3.605);
			instance1.setPaid(0);
			Factory.GetInstance().getTicketsDAO().add(instance1);
			instance2 = Factory.GetInstance().getTicketsDAO().getById(1);
			instance2.setTicket_id(466);
			instance2.setUser(user);
			instance2.setCost(1.605);
			instance2.setPaid(1.605);
			Factory.GetInstance().getTicketsDAO().add(instance2);
			List<Tickets> result = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByUser(user, true);
			List<Tickets> result_n = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByUser(user, false);
			assertNotNull(result);
			assertEquals(result.size(), old.size() + 2);
			assertNotNull(result_n);
			assertEquals(result_n.size(), old_n.size() + 1);
		} finally {
			if (instance0 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance0);
			if (instance1 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance1);
			if (instance2 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance2);
		}
	}

	@Test
	public void getByFlight0() {
		Tickets instance0 = null, instance1 = null;
		try {
			Flights flight = Factory.GetInstance().getFlightsDAO().getById(1);
			assertNotNull(flight);
			List<Tickets> old = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByFlight(flight, null);
			instance0 = Factory.GetInstance().getTicketsDAO().getById(1);
			assertNotNull(instance0);
			instance0.setTicket_id(456);
			instance0.setFlight(flight);
			Factory.GetInstance().getTicketsDAO().add(instance0);
			instance1 = Factory.GetInstance().getTicketsDAO().getById(1);
			instance1.setTicket_id(466);
			instance1.setFlight(flight);
			Factory.GetInstance().getTicketsDAO().add(instance1);
			List<Tickets> result = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByFlight(flight, null);
			assertNotNull(result);
			assertEquals(result.size(), old.size() + 2);
		} finally {
			if (instance0 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance0);
			if (instance1 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance1);
		}
	}

	@Test
	public void getByFlight1() {
		Tickets instance0 = null, instance1 = null;
		try {
			Flights flight = Factory.GetInstance().getFlightsDAO().getById(1);
			assertNotNull(flight);
			List<Tickets> old = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByFlight(flight, 1);
			instance0 = Factory.GetInstance().getTicketsDAO().getById(1);
			assertNotNull(instance0);
			instance0.setTicket_id(456);
			instance0.setFlight(flight);
			instance0.setType(1);
			Factory.GetInstance().getTicketsDAO().add(instance0);
			instance1 = Factory.GetInstance().getTicketsDAO().getById(1);
			instance1.setTicket_id(466);
			instance1.setFlight(flight);
			instance1.setType(2);
			Factory.GetInstance().getTicketsDAO().add(instance1);
			List<Tickets> result = (List<Tickets>)Factory.GetInstance().getTicketsDAO().getByFlight(flight, 1);
			assertNotNull(result);
			assertEquals(result.size(), old.size() + 1);
		} finally {
			if (instance0 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance0);
			if (instance1 != null)
				Factory.GetInstance().getTicketsDAO().delete(instance1);
		}
	}
}
