package org.badcoding.test;


import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UsersDAOTest extends Assert {

	@Test
	public void add() {
		Users instance = null;
		try {
			instance = Factory.GetInstance().getUsersDAO().getById(1);
			assertNotNull(instance);
			instance.setUser_id(6789);
			Factory.GetInstance().getUsersDAO().add(instance);
			Users inserted = Factory.GetInstance().getUsersDAO().getById(6789);
			assertNotNull(inserted);
			assertEquals(inserted.getUser_id(), instance.getUser_id());
		} catch (Exception e) {
			if (instance != null)
				Factory.GetInstance().getUsersDAO().delete(instance);
			throw e;
		}
	}
	
	@Test(dependsOnMethods = { "update" })
	public void delete() {
		Users inserted = Factory.GetInstance().getUsersDAO().getById(6789);
		Factory.GetInstance().getUsersDAO().delete(inserted);
		Users removed = Factory.GetInstance().getUsersDAO().getById(6789);
		assertNull(removed);
	}
	
	@Test(dependsOnMethods = { "add" })
	public void update() {
		Users inserted = null;
		try {
			inserted = Factory.GetInstance().getUsersDAO().getById(6789);
			inserted.setEmail("me@heaven.org");
			Factory.GetInstance().getUsersDAO().update(inserted);
			Users updated = Factory.GetInstance().getUsersDAO().getById(6789);
			assertEquals("me@heaven.org", updated.getEmail());
		} catch (Exception e) {
			if (inserted != null)
				Factory.GetInstance().getUsersDAO().delete(inserted);
			throw e;
		}
	}

}
