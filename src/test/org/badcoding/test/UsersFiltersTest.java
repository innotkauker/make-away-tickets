package org.badcoding.test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.badcoding.hibernate.logic.Factory;
import org.badcoding.hibernate.stored.*;
import org.testng.Assert;
import org.testng.annotations.*;

public class UsersFiltersTest extends Assert {
	@DataProvider
	public Object[][] filters_test_data() {
		return new Object[][]{
			{null, null, null, -1, -1, -1, null, 0, 3},
			{"name", null, null, -1, -1, -1, null, 1, 3},
			{"1", null, null, -1, -1, -1, null, 2, 1},
			{"2", "sn", null, -1, -1, -1, null, 3, 1},
			{null, "sn", null, -1, -1, -1, null, 4, 3},
			{"lol", "sn", null, -1, -1, -1, null, 5, 0},
			{null, null, null, -1, -1, -1, "5678@ya.hu", 6, 3},
			{"name1", "sn1", "p1", -1, -1, -1, "5678@ya.hu", 7, 1},
			{null, null, null, -1, -1, 0, null, 8, 1},
			{"2", null, null, -1, -1, 0, null, 9, 0},
			{null, null, null, -1, -1, 1, null, 10, 2},
			{null, null, "3", 254, -1, -1, null, 11, 0},
			{"name", null, null, 234, -1, -1, null, 12, 2},
			{null, null, null, 244, -1, -1, null, 13, 0},
			{null, "2", null, -1, 909, -1, null, 14, 0},
			{null, null, null, -1, 999, -1, null, 15, 0},
			{null, null, "2", -1, 900, -1, null, 16, 1},
			{null, null, null, 234, 900, -1, null, 17, 2},
			{null, null, null, -1, 900, 0, null, 18, 1},
			{null, null, null, -1, 999, 0, null, 19, 0},
			{null, null, null, -1, -1, 0, "5678@ya.hu", 20, 1},
		};
	}
	private static List<Integer> init_stats = new ArrayList<Integer>();

	@Test(dependsOnMethods = { "get_initial_statistics" })
	public void add_data() {
		add_user(5678, "name1", "sn1", "p1", "5678@ya.hu");
		add_user(5679, "name2", "sn2", "p2", "5678@ya.hu");
		add_user(5688, "name3", "sn3", "p3", "5678@ya.hu"); // free
		add_company(900);
		add_company(909);
		add_company(999); // free
		add_flight(234, 900);
		add_flight(244, 909); // free
		add_flight(254, 909);
		add_flight(235, 900);
		add_ticket(8989, 234, 5678, 0.5, 0.51);
		add_ticket(856, 234, 5678, 0.5, 0.41);
		add_ticket(8966, 254, 5678, 0.6, 0.51);
		add_ticket(8983, 235, 5678, 0.5, 0.51);
		add_ticket(1983, 235, 5679, 0.5, 0.51);
		add_ticket(4983, 234, 5679, 0.5, 0.51);
	}

	private void add_user(int id, String first_name, String last_name, String patronymic, String email) {
		Users instance = Factory.GetInstance().getUsersDAO().getById(1);
		assertNotNull(instance);
		instance.setUser_id(id);
		instance.setName(first_name);
		instance.setLast_name(last_name);
		instance.setPatronymic(patronymic);
		instance.setEmail(email);
		Factory.GetInstance().getUsersDAO().add(instance);
	}

	private void add_company(int id) {
		Companies instance = Factory.GetInstance().getCompaniesDAO().getById(1);
		assertNotNull(instance);
		instance.setCompany_id(id);
		Factory.GetInstance().getCompaniesDAO().add(instance);
	}

	private void add_flight(int id, int company) {
		Flights instance = Factory.GetInstance().getFlightsDAO().getById(1);
		assertNotNull(instance);
		instance.setFlight_id(id);
		instance.setCompany(Factory.GetInstance().getCompaniesDAO().getById(company));
		Factory.GetInstance().getFlightsDAO().add(instance);
	}

	private void add_ticket(int id, int flight, int user, double cost, double paid) {
		Tickets instance = Factory.GetInstance().getTicketsDAO().getById(1);
		assertNotNull(instance);
		instance.setTicket_id(id);
		instance.setUser(Factory.GetInstance().getUsersDAO().getById(user));
		instance.setFlight(Factory.GetInstance().getFlightsDAO().getById(flight));
		instance.setCost(cost);
		instance.setPaid(paid);
		Factory.GetInstance().getTicketsDAO().add(instance);
	}

	@Test(dataProvider = "filters_test_data")
	public void get_initial_statistics(String first_name, String last_name, String patronymic, int flight_id, int company_id, int paid, String email, int id, int delta) {
		Collection<Users> result = Factory.GetInstance().getUsersDAO().getByFilters(first_name, last_name, patronymic, flight_id, company_id, paid, email);
		if (result == null)
			init_stats.add(0);
		else
			init_stats.add(result.size());
	}

	@Test(dataProvider = "filters_test_data", dependsOnMethods = { "add_data" })
	public void review_changes(String first_name, String last_name, String patronymic, Integer flight_id, Integer company_id, Integer paid, String email, int i, int diff) {
		Collection<Users> result = Factory.GetInstance().getUsersDAO().getByFilters(first_name, last_name, patronymic, flight_id, company_id, paid, email);
		System.out.println(i + ": " + result.size());
		assertEquals(result.size(), (int)init_stats.get(i) + diff);
	}

	@AfterClass
	public void clean() {
		remove_ticket(8989);
		remove_ticket(856);
		remove_ticket(8966);
		remove_ticket(8983);
		remove_ticket(1983);
		remove_ticket(4983);
		remove_flight(234);
		remove_flight(244);
		remove_flight(254);
		remove_flight(235);
		remove_company(900);
		remove_company(909);
		remove_company(999);
		remove_user(5678);
		remove_user(5679);
		remove_user(5688);
	}

	private void remove_user(int id) {
		Users inserted = Factory.GetInstance().getUsersDAO().getById(id);
		if (inserted != null)
			Factory.GetInstance().getUsersDAO().delete(inserted);
	}

	private void remove_ticket(int id) {
		Tickets inserted = Factory.GetInstance().getTicketsDAO().getById(id);
		if (inserted != null)
			Factory.GetInstance().getTicketsDAO().delete(inserted);
	}

	private void remove_flight(int id) {
		Flights inserted = Factory.GetInstance().getFlightsDAO().getById(id);
		if (inserted != null)
			Factory.GetInstance().getFlightsDAO().delete(inserted);
	}

	private void remove_company(int id) {
		Companies inserted = Factory.GetInstance().getCompaniesDAO().getById(id);
		if (inserted != null)
			Factory.GetInstance().getCompaniesDAO().delete(inserted);
	}
}
